# Guides web des Salles des marchés

## Installation

Ces guides sont créés avec [Hugo](https://gohugo.io/). Pour s'en servir, il suffit d'installer `hugo` en suivant [ces instructions](https://gohugo.io/getting-started/installing).

## HTML

Pour créer le dossier `public`, il suffit d'exécuter la commande `hugo` à partir de ce dossier :

```bash
user@host $ hugo
```

La commande `hugo server` permet de lancer un serveur de développement.

> Ceci est expliqué en détail dans le guide de démarrage rapide disponible [ici](https://gohugo.io/getting-started/quick-start).


## Édition

Les fichiers `.md` avec le contenu des guides se trouvent sous le dossier `content/`. Par exemple, pour éditer le contenu du guide `http://localhost:1313/latrousse/fr/bloomberg/bba/web/`, il suffit de modifier le texte du fichier `content/bloomberg/bba/web/_index.fr.md`.


> Un fichier `.md` doit utiliser la syntaxe [Markdown Goldmark](https://gohugo.io/getting-started/configuration-markup).


