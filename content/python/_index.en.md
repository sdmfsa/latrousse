---
title: "Python"
date: 2020
icon: "fab fa-python"
description: "Some code examples in Python: installing, charts, etc."
keywords: python, installation, installer, jupyter, notebook, chart, plot, matplotlib, mplfinance, plotly
type : "docs"
weight: 1
---

