---
title: Linear regression
keywords: regression, linear, python, chart, beta, matplotlib, scikit-learn, pandas, numpy
description: How to do a simple linear regression with Python.
weight: 0
---

## BETA

In this example we will find the value of the slope $\beta$ in a linear regression $y = \alpha + \beta x$, where the variables correspond to :

+ $x$ : the returns of the `S&P/TSX Composite Index`.
+ $y$ : the returns of the `BlackBerry Limited` stock.

To find the slope and intercept, we use the `LinearRegression` object from `sklearn.linear_model` {{%extlink "https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html"%}}.

{{%tabs%}}

{{%tab "Code"%}}

### scikit-learn

1. Import the $y$ and $x$ data :

```python
import pandas as pd
from sklearn.linear_model import LinearRegression
df = pd.read_csv("bb_sptsx.csv")  # weekly returns 2019-09-06 to 2021-08-20
```

2. Each variable should be filled by column. Here we use the `numpy.reshape` function {{%extlink "https://numpy.org/doc/stable/reference/generated/numpy.reshape.html"%}} to make convert each vector of length $n$ into a $n \times 1$ matrix.

```python
x_data = df['SPTSX'].values.reshape(-1, 1)
y_data = df['BB'].values.reshape(-1, 1)
```

3. Use the `LinearRegression().fit` function for the linear regression $y = \alpha + \beta x$ :

```python
reg = LinearRegression()
reg.fit(x_data, y_data)
print(reg.coef_)  # La valeur de beta
print(reg.intercept_)  # La valeur de alpha
```

{{%notice tip%}}
To create a regression chart, the first step is to create a standard scatter plot, e.g. with the `scatter()` function from `matplotlib`. Next, we can add a regression line obtained with `LinearRegression.predict()` using the `plot()` function.
Additional packages are available for regression analysis online {{%extlink  "https://seaborn.pydata.org/tutorial/regression.html"%}}.
{{%/notice%}}

{{%/tab%}}

{{%tab "Jupyter Notebook"%}}
![](reg_jupyter.gif)
{{%/tab%}}


{{%/tabs%}}

