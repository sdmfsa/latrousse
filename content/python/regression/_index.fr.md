---
title: Régression linéaire
keywords: régression, linéaire, python, graphique, beta, matplotlib, scikit-learn, pandas, numpy
description: Comment faire une régression linéaire avec Python.
weight: 0
---

## BETA

Nous allons trouver la valeur de la pente $\beta$ d'une régression du type $y = \alpha + \beta x$, où les valeurs des variables sont :

+ $x$ : les rendements de l'indice `S&P/TSX Composite Index`.
+ $y$ : les rendements du titre `BlackBerry Limited`.

Pour trouver la pente et l'ordonnée à l'origine, on utilisera un objet `LinearRegression` de `sklearn.linear_model` {{%extlink "https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html"%}}.

{{%tabs%}}

{{%tab "Code"%}}

### scikit-learn

1. Importez les données correspondant aux variables $y$ et $x$ :

```python
import pandas as pd
from sklearn.linear_model import LinearRegression
df = pd.read_csv("bb_sptsx.csv")  # rendements hebdomadaires 2019-09-06 à 2021-08-20
```

2. Chaque variable doit être arrangée par colonne. Ici nous utilisons la fonction `numpy.reshape` {{%extlink "https://numpy.org/doc/stable/reference/generated/numpy.reshape.html"%}} pour convertir chaque vecteur de longueur $n$ dans une matrice de taille $n \times 1$ :

```python
x_data = df['SPTSX'].values.reshape(-1, 1)
y_data = df['BB'].values.reshape(-1, 1)
```

3. Utilisez la fonction `LinearRegression().fit` pour faire la régression linéaire $y = \alpha + \beta x$.

```python
reg = LinearRegression()
reg.fit(x_data, y_data)
print(reg.coef_)  # La valeur de beta
print(reg.intercept_)  # La valeur de alpha
```

{{%notice tip%}}
Pour créer un graphique de cette régression, il suffit de créer un nuage de points avec la fonction `scatter()` de `matplotlib`. Ensuite, on ajoute la ligne de régression obtenue avec `LinearRegression.predict()` à l'aide de la fonction `plot()`.
D'autres paquetages sont disponibles pour faire des analyses de régression {{%extlink  "https://seaborn.pydata.org/tutorial/regression.html"%}}.
{{%/notice%}}

{{%/tab%}}

{{%tab "Jupyter Notebook"%}}
![](reg_jupyter.gif)
{{%/tab%}}


{{%/tabs%}}

