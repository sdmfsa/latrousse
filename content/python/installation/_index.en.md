---
title: Installation
keywords: how, to, install, python, jupyter, notebook
description: Installing Python and Jupyter Notebook.
weight: 0
---

Follow these instruction to install `Python` as well as the `Jupyter Notebook` interactive development environment using the `Anaconda` distribution {{%extlink "https://code.visualstudio.com/download"%}}. If you prefer to install an integrated development environment without installing `Anaconda`, you can follow the VS Code installation instructions [here](#vscode) instead.

## Download {#download}

{{%notice note%}}
Python is available on `Windows`, `MAC` and `Linux`. You can use  **Python** without **Jupyter Notebook**, but not the opposite.
{{%/notice%}}

{{%tabs%}}
{{%tab "Instructions"%}}


### Anaconda

> This software includes both  **Python** and **Jupyter Notebook**, which eases the installation.


1. Go to `https://www.anaconda.com/products/individual` {{%extlink "https://www.anaconda.com/products/individual"%}}.
2. Click on `Download` and get the recommended file for your operating system.
3. Open the downloaded file and follow the installation instructions.

### Jupyter Notebook

1. Launch `Anaconda Navigator`, which was installed with `Anaconda`.
2. The `Jupyter Notebook` should already be installed, otherwise click on `Install`.

{{%/tab%}}

{{%tab "Anaconda"%}}
![](install_anaconda.gif)
{{%/tab%}}


{{%tab "Jupyter Notebook"%}}
![](install_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}

{{%notice tip%}}
Check the official documentation to learn more about the `Jupyter Notebook` interface and shortcuts <a href="https://jupyter-notebook.readthedocs.io/en/stable/notebook.html?highlight=shortcuts#keyboard-shortcuts" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>.
{{%/notice%}}


## Python or Jupyter Notebook

Let's test our Python and Jupyter Notebook installation through Anaconda. Jupyter Notebook is an interactive development environment which helps us creating programs with `Python`, `Julia`, `R` and others. Here's a comparison of using Python with and without Jupyter Notebook.

{{%tabs%}}
{{%tab "Instructions"%}}

### Python

1. Launch a command line interface or terminal.
2. Launch `Python` with the `python` command.
3. Enter `print("Allô")` on the command line and hit `<Enter>`.

### Jupyter Notebook

1. Launch Jupyter Notebook.
2. Enter `print("Allô")` on an empty cell.
3. Click on `Run`.

{{%/tab%}}

{{%tab "Python"%}}
![](allo_python.gif)
{{%/tab%}}


{{%tab "Jupyter Notebook"%}}
![](allo_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}

## Visual Studio Code {#vscode}


Visual Studio Code is a popular integrated development environment or IDE {{%extlink "https://en.wikipedia.org/wiki/Integrated_development_environment"%}}. We can use VS Code with several programming languages by installing the appropriate recommended tools.

{{%notice info%}}
VS Code comes pre-installed with JavaScript, TypeScript, CSS and HTML. For any other language, we need to install additional extensions through the VS Code Marketplace {{%extlink "https://marketplace.visualstudio.com/"%}}.

{{%/notice%}}

{{%tabs%}}
{{%tab "Instructions"%}}

### Download

1. Go to the VS code website at `https://code.visualstudio.com/download` {{%extlink "https://code.visualstudio.com/download"%}}.
2. Click on your operating system icon to download the recommended installation file.
3. Open the downloaded file and follow the instructions as usual.

### Python extension

1. Create a new Python file.
1. Install the recommended Python extension through  VS Code.
1. Run the code by clicking on the `Run and Debug` button. The program output will appear in the command line.

{{%/tab%}}

{{%tab "Visual Studio"%}}

### Download

![](install_vscode.gif)

### Python extension

![](hello_vscode.gif)
{{%/tab%}}


{{%/tabs%}}





## Packages

`Python` offers more than **300 000** packages through the *Python Package Index* <a href="https://pypi.org/" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.

{{%tabs%}}
{{%tab "Instructions"%}}

### Python

1. Launch a command line interface or terminal.
2. Use `pip install` to install a package, e.g. `pip install numpy`.

### Jupyter Notebook

1. Launch Jupyter Notebook.
2. Use `! pip install` to install a package, e.g. `! pip install numpy`.

{{%/tab%}}

{{%tab "Python"%}}
![](pip_python.gif)
{{%/tab%}}


{{%tab "Jupyter Notebook"%}}
![](pip_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}

