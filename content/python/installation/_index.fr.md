---
title: Installation
keywords: comment, installer, python, jupyter, notebook
description: Comment installer Python et Jupyter Notebook.
weight: 0
---

Suivez ces instructions pour installer `Python` ainsi que l'environnement de développement `Jupyter Notebook` à l'aide de la distribution `Anaconda` {{%extlink "https://code.visualstudio.com/download"%}}. Si vous préférez installer un environnement de développement intégré sans avoir à installer `Anaconda`, suivez plutôt les instructions d'installation pour VS Code qui se trouvent [ici](#vscode).

## Téléchargement {#download}

{{%notice note%}}
Python est disponible sur `Windows`, `MAC` et `Linux`. Vous pouvez utiliser **Python** sans **Jupyter Notebook**, mais pas l'inverse.
{{%/notice%}}

{{%tabs%}}
{{%tab "Instructions"%}}


### Anaconda

> Ce logiciel inclut **Python** et **Jupyter Notebook**, ce qui facilite l'installation et l'entretien.

1. Allez sur le site `https://www.anaconda.com/products/individual` {{%extlink "https://www.anaconda.com/products/individual"%}}.
2. Cliquez sur `Download` pour téléchargez le fichier d'installation recommandé selon votre système d'exploitation.
3. Ouvrez le fichier téléchargé et suivez les instructions d'installation.

### Jupyter Notebook

1. Lancez l'application `Anaconda Navigator`, installée avec `Anaconda`.
2. Le logiciel `Jupyter Notebook` devrait déjà être installé avec Anaconda, sinon cliquez sur `Install`.

{{%/tab%}}

{{%tab "Anaconda"%}}
![](install_anaconda.gif)
{{%/tab%}}


{{%tab "Jupyter Notebook"%}}
![](install_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}

{{%notice tip%}}
Pour vous familiariser avec l'interface et les raccourcis de `Jupyter Notebook`, consultez la documentation <a href="https://jupyter-notebook.readthedocs.io/en/stable/notebook.html?highlight=shortcuts#keyboard-shortcuts" target="_blank"><i class="fas fa-external-link-square-alt"></i></a>.
{{%/notice%}}


## Python ou Jupyter Notebook

Testons notre installation de Python ainsi que celle de Jupyter Notebook via Anaconda. Jupyter Notebook est un environnement de développement interactif qui nous aide à créer des programmes avec `Python`, `Julia`, `R` et autres.  Voici une comparaison de l'interface de travail avec Python vs Jupyter Notebook.

{{%tabs%}}
{{%tab "Instructions"%}}

### Python

1. Lancez l'invite de commande.
2. Lancez `Python` avec la commande `python`.
3. Rentrez `print("Allô")` dans l'invite de commande.

### Jupyter Notebook

1. Lancez Jupyter Notebook.
2. Rentrez `print("Allô")` dans une cellule vide.

{{%/tab%}}

{{%tab "Python"%}}
![](allo_python.gif)
{{%/tab%}}


{{%tab "Jupyter Notebook"%}}
![](allo_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}


## Visual Studio Code{#vscode}

Visual Studio Code est un environnement de développement intégré ou IDE {{%extlink "https://en.wikipedia.org/wiki/Integrated_development_environment"%}} très populaire pour développer avec plusieurs langages de programmation. Pour l'utiliser, il suffit de se procurer une copie du logiciel en ligne et de l'installer sur notre ordinateur comme tout autre logiciel. Ensuite, il faut s'assurer d'installer les  outils recommandés selon le langage que nous allons utiliser.

{{%notice info%}}
VS code est configuré par défaut pour travailler avec JavaScript, TypeScript, CSS et HTML. Tout autre langage additionnel doit être installé via le VS Code Marketplace {{%extlink "https://marketplace.visualstudio.com/"%}}.

{{%/notice%}}

{{%tabs%}}
{{%tab "Instructions"%}}

### Téléchargement

1. Allez sur le site `https://code.visualstudio.com/download` {{%extlink "https://code.visualstudio.com/download"%}}.
2. Cliquez sur votre système d'exploitation pour télécharger le fichier d'installation recommandé.
3. Ouvrez le fichier téléchargé et suivez les instructions d'installation.

### Extension Python

1. Créez un nouveau fichier Python.
1. Installez l'extension Python recommandée par VS Code.
1. Exécutez le code du fichier en cliquant sur `Run and Debug`. L'output apparaitra dans l'invite de commande.

{{%/tab%}}

{{%tab "Visual Studio"%}}

### Téléchargement

![](install_vscode.gif)

### Extension Python

![](hello_vscode.gif)
{{%/tab%}}


{{%/tabs%}}



## Paquetages

`Python` vous offre plus de **300 000** paquetages via le *Python Package Index* <a href="https://pypi.org/" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.

{{%tabs%}}
{{%tab "Instructions"%}}

### Python

1. Lancez l'invite de commande.
2. Utilisez `pip install` pour installer un paquetage, e.g. `pip install numpy`.

### Jupyter Notebook

1. Lancez Jupyter Notebook.
2. Utilisez `! pip install` pour installer un paquetage, e.g. `! pip install numpy`.

{{%/tab%}}

{{%tab "Python"%}}
![](pip_python.gif)
{{%/tab%}}


{{%tab "Jupyter Notebook"%}}
![](pip_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}

