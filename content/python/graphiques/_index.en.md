---
title: Charts
keywords: charts, python, plot, plotly, mplfinance, histogram, matplotlib
description: Candlestick charts and histograms with Python.
weight: 0
---

## Financial OHLC

To build a financial OHLC chart (Open, High, Low, Close) using `Python`, we may choose among several available packages (matplotlib, plotly, etc.). Here we will compare the output obtained using `plotly` {{%extlink "https://plotly.com/python/candlestick-charts/"%}} vs `mplfinance` {{%extlink "https://github.com/matplotlib/mplfinance"%}}, but the overall syntax is similar when using other packages.


> This example uses the same data obtained in the [CSV files](../csv/) section.

{{%tabs%}}

{{%tab "Code"%}}

### plotly

```python
import plotly.graph_objects as go
import pandas as pd
df = pd.read_csv("BB.TO.csv")
fig = go.Figure(data=[go.Candlestick(x=df['Date'],
                open=df['Open'],
                high=df['High'],
                low=df['Low'],
                close=df['Close'])])
fig.update_layout(
    title='Prix historiques',
    yaxis_title='Action BB.TO'
)
fig.show()
```

### mplfinance

```python
import mplfinance as mpf
import pandas as pd
df = pd.read_csv("BB.TO.csv", index_col=0, parse_dates=True)
df.index.name = 'Date'
mpf.plot(df, type='candle', title = 'Action BB.TO')
```

{{%/tab%}}

{{%tab "plotly"%}}
![](plotly_output.gif)
{{%/tab%}}

{{%tab "mplfinance"%}}
![](mplfinance_output.gif)
{{%/tab%}}

{{%/tabs%}}

## Histogram

Now let's visualize the empirical distribution of a financial security's returns using an histogram. Here we compare the output obtained using two packages : `plotly` vs `matplotlib` using the prices downloaded [here](../csv/).

{{%tabs%}}

{{%tab "Code"%}}

### matplotlib - hist

1. Compute the returns using the prices in the `Adj Close` column.
1. Create an histogram using the above returns and the `hist` function from the `matplotlib` package.
1. Add a title using the `title` function.

```python
import matplotlib.pyplot as plt
plt.hist(df['Adj Close'].pct_change())
plt.title("Rendements de BB.TO")
```

### plotly - histogram

1. Compute the returns using the prices in the `Adj Close` column.
1. Create an histogram using the above returns and the `histogram` function from the `plotly` package.
1. Add a title using the `update_layout` function.

```python
import plotly.express as px
df = df['Adj Close'].pct_change()
fig = px.histogram(df, x="Adj Close")
fig.update_layout(
    title='Rendements de BB.TO',
    yaxis_title='Fréquence'
)
fig.show()
```

{{%notice tip%}}
The buttons on the top right corner of the Plotly output allow you to download a copy of the chart.
{{%/notice%}}


{{%/tab%}}

{{%tab "matplotlib"%}}
![](hist_matplotlib.gif)
{{%/tab%}}


{{%tab "plotly"%}}
![](hist_plotly.gif)
{{%/tab%}}

{{%/tabs%}}



