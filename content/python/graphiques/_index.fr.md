---
title: Graphiques
keywords: graphiques, r, charts, ggplot2, plot, histogramme
description: Création de graphiques avec Python et ggplot2.
weight: 0
---

## Boursier OHLC

Pour créer des graphiques de type OHLC (Open, High, Low, Close) sur `Python`, on peut choisir parmi plusieurs paquetages disponibles (matplotlib, plotly, etc.). Ici nous allons comparer l'output obtenu avec `plotly` {{%extlink "https://plotly.com/python/candlestick-charts/"%}} vs `mplfinance` {{%extlink "https://github.com/matplotlib/mplfinance"%}}, mais la syntaxe est semblable avec d'autres paquetages.


> Cet exemple utilise les mêmes données trouvées dans la section [Fichiers CSV](../csv/).

{{%tabs%}}

{{%tab "Code"%}}

### plotly

```python
import plotly.graph_objects as go
import pandas as pd
df = pd.read_csv("BB.TO.csv")
fig = go.Figure(data=[go.Candlestick(x=df['Date'],
                open=df['Open'],
                high=df['High'],
                low=df['Low'],
                close=df['Close'])])
fig.update_layout(
    title='Prix historiques',
    yaxis_title='Action BB.TO'
)
fig.show()
```

### mplfinance

```python
import mplfinance as mpf
import pandas as pd
df = pd.read_csv("BB.TO.csv", index_col=0, parse_dates=True)
df.index.name = 'Date'
mpf.plot(df, type='candle', title = 'Action BB.TO')
```

{{%/tab%}}

{{%tab "plotly"%}}
![](plotly_output.gif)
{{%/tab%}}

{{%tab "mplfinance"%}}
![](mplfinance_output.gif)
{{%/tab%}}

{{%/tabs%}}

## Histogramme

Visualisons maintenant la distribution empirique des rendements d'un titre à l'aide d'un histogramme. Pour ce faire, nous allons comparer l'output de deux librairies : `plotly` vs `matplotlib` avec les prix téléchargés [ici](../csv/).

{{%tabs%}}

{{%tab "Code"%}}

### matplotlib - hist

1. Calculez les rendements à l'aide de la colonne de prix `Adj Close`.
1. Créez l'histogramme des rendements avec la commande `hist` du paquetage `matplotlib`.
1. Ajoutez un titre avec la commande `title`.

```python
import matplotlib.pyplot as plt
plt.hist(df['Adj Close'].pct_change())
plt.title("Rendements de BB.TO")
```

### plotly - histogram

1. Calculez les rendements à l'aide de la colonne de prix `Adj Close`.
1. Créez l'histogramme des rendements avec la commande `histogram` du paquetage `plotly`.
1. Ajoutez un titre avec la commande `update_layout`.

```python
import plotly.express as px
df = df['Adj Close'].pct_change()
fig = px.histogram(df, x="Adj Close")
fig.update_layout(
    title='Rendements de BB.TO',
    yaxis_title='Fréquence'
)
fig.show()
```

{{%notice tip%}}
Les boutons disponibles sur le graphique Plotly vous permettent d'enregistrer une copie.
{{%/notice%}}


{{%/tab%}}

{{%tab "matplotlib"%}}
![](hist_matplotlib.gif)
{{%/tab%}}


{{%tab "plotly"%}}
![](hist_plotly.gif)
{{%/tab%}}

{{%/tabs%}}



