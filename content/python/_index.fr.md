---
title: "Python"
date: 2020
icon: "fab fa-python"
description: "Quelques exemples de code en Python: installation, création de graphiques, etc."
keywords: python, installation, installer, jupyter, notebook, graphique
type : "docs"
weight: 1
---

