---
title: CSV files
keywords: read, files, csv, data, python, import
description: Reading a CSV file with Python
weight: 0
---

## Importing data



To read a `.csv` file with `Python` we will use the `read_csv` function form the `pandas` package. This function takes the file name as first argument, e.g. `mon_fichier.csv`. If you data is not comma delimited, you may use the `sep` argument to indicate the correct delimiter, e.g. `sep=";"`:

```python
import pandas as pd
donnees <- pd.read_csv("mon_fichier.csv", sep=",")
```

{{%tabs%}}

{{%tab "Instructions"%}}

1. Download historical prices for `BB.TO` from `https://ca.finance.yahoo.com/`.

2. Read the `CSV` data using the `pandas.read_csv()` function and save this data to a variable named `donnees`:

```python
import pandas as pd
donnees = pd.read_csv("~/Downloads/BB.TO.csv")
```

{{%/tab%}}

{{%tab "Jupyter Notebook"%}}
![](csv_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}

