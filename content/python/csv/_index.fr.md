---
title: Fichiers CSV
keywords: lire, fichier, csv, données, python, importer
description: Comment lire un fichier CSV avec Python
weight: 0
---

## Importation des données

Pour lire un fichier `.csv` sur `Python` on peut utiliser la fonction `read_csv` du paquetage `pandas`. Cette fonction prend comme premier argument le nom du fichier, par exemple `mon_fichier.csv`. Si le délimiteur des données n'est pas une virgule, il suffit de l'indiquer avec l'argument `sep` :

```python
import pandas as pd
donnees <- pd.read_csv("mon_fichier.csv", sep=",")
```

{{%tabs%}}

{{%tab "Instructions"%}}

1. Obtenez les prix historiques de `BB.TO` sur le site `https://ca.finance.yahoo.com/`.

2. Lisez le tableau de données `CSV` avec la fonction `pandas.read_csv()` et sauvegardez-le dans une variable nommée `donnees` :

```python
import pandas as pd
donnees = pd.read_csv("~/Downloads/BB.TO.csv")
```

{{%/tab%}}

{{%tab "Jupyter Notebook"%}}
![](csv_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}

