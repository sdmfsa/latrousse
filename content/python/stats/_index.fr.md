---
title: Statistiques
keywords: statistiques descriptives, moyenne, variance, quantiles, sommaire, pandas, taille
description: "Python : calcul de statistiques descriptives."
weight: 0
---

Pour ces exemples, nous allons utiliser un tableau `pandas.DataFrame` à deux colonnes correspondant aux rendements hebdomadaires de l'indice *S&P/TSX Composite* et de l'action de *BlackBerry Limited* entre 2019-09-06 et 2021-08-20.

{{%notice tip%}}
Une liste des méthodes disponibles avec un tableau `pandas` est disponible sur la documentation officielle {{%extlink "https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf"%}}.
{{%/notice%}}

## Sommaire

{{%tabs%}}

{{%tab "Code"%}}

1. Lisez vos données sur Python avec `read_csv` et nommez le tableau `mydata`.

```python
import pandas as pd
mydata = pd.read_csv("bb_sptsx.csv")
```

1. Obtenez un sommaire du `DataFrame` avec la méthode `describe()`. Par défaut, vous obtiendrez des statistiques descriptives {{%extlink "https://en.wikipedia.org/wiki/Descriptive_statistics"%}} de chaque colonne **numérique** {{%extlink "https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.describe.html"%}}.

```python
mydata.describe()
```

1. Vérifiez la taille du tableau avec l'attribut `shape`.

```python
mydata.shape
```

{{%/tab%}}

{{%tab "Jupyter Notebook"%}}
![](summary_jupyter.gif)
{{%/tab%}}


{{%/tabs%}}

## Moyenne et variance

{{%tabs%}}

{{%tab "Code"%}}

1. Obtenez la moyenne de la colonne `BB` avec la méthode `mean()`.

```python
mydata['BB'].mean()
```
1. Obtenez la variance échantillonnale de la colonne `SPTSX` avec la méthode `var()`.

```python
mydata['SPTSX'].var()
```
1. Obtenez la moyenne de chaque colonne numérique de `mydata` avec la méthode `mean()`.

```python
mydata.mean()
```

1. Obtenez la variance de chaque colonne numérique de `mydata` à l'aide de la méthode `var()`.

```python
mydata.var()
```

> Puisque la colonne `Date` n'est pas numérique mais plutôt une chaîne de caractères, elle est ignorée par défaut pour les calculs de type numérique.

{{%/tab%}}

{{%tab "Jupyter Notebook"%}}
![](mean-var_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}


## Covariance

{{%tabs%}}

{{%tab "Code"%}}

1. Obtenez la covariance entre les colonnes `BB` et `SPTSX` du tableau `mydata` avec la méthode `cov()` {{%extlink "https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.cov.html"%}}.

```python
mydata.cov()
```

1. Sauvegardez la matrice variance-covariance résultante dans un fichier CSV.


```python
mydata.cov().to_csv("covariance.csv")
```

{{%/tab%}}

{{%tab "Jupyter Notebook"%}}
![](covariance_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}

