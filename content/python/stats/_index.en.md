---
title: Statistics
keywords: descriptive, statistics, mean, variance, quantiles, summary, pandas, size
description: "Descriptive statistics with Python."
weight: 0
---

For this example, we will use a `pandas.DataFrame` to store a two columns table of weekly returns corresponding to the *S&P/TSX Composite Index* and the stock of *BlackBerry Limited* between 2019-09-06 and 2021-08-20.

{{%notice tip%}}
A list of available methods for a `pandas` table can be accessed through the online documentation {{%extlink "https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf"%}}.
{{%/notice%}}

## Summary

{{%tabs%}}

{{%tab "Code"%}}

1. Read the data on Python using `read_csv` and name your variable `mydata`.

```python
import pandas as pd
mydata = pd.read_csv("bb_sptsx.csv")
```

1. Get a summary of the `DataFrame` using the `describe()` method.  By default, you will receive descriptive statistics {{%extlink "https://en.wikipedia.org/wiki/Descriptive_statistics"%}} for the numeric columns of your table {{%extlink "https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.describe.html"%}}.

```python
mydata.describe()
```

1. Check the table dimensions with the `shape` attribute.

```python
mydata.shape
```

{{%/tab%}}

{{%tab "Jupyter Notebook"%}}
![](summary_jupyter.gif)
{{%/tab%}}


{{%/tabs%}}

## Mean and variance

{{%tabs%}}

{{%tab "Code"%}}

1. Find the mean value of the `BB` column using the `mean()` method.

```python
mydata['BB'].mean()
```
1. Find the variance of the `SPTSX` column using the `var()` method.

```python
mydata['SPTSX'].var()
```
1. Find the mean of each numeric column of `mydata` using the `mean()` method.

```python
mydata.mean()
```

1. Find the variance of each numeric column of `mydata` using the `var()` method.

```python
mydata.var()
```

> The `Date` column is ignored since it is not numeric but a string of characters.

{{%/tab%}}

{{%tab "Jupyter Notebook"%}}
![](mean-var_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}


## Covariance

{{%tabs%}}

{{%tab "Code"%}}

1. Find the covariance matrix between `BB` and `SPTSX` using the `mydata` table and the `cov()` method {{%extlink "https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.cov.html"%}}.

```python
mydata.cov()
```

1. Save the matrix data to a CSV file.


```python
mydata.cov().to_csv("covariance.csv")
```

{{%/tab%}}

{{%tab "Jupyter Notebook"%}}
![](covariance_jupyter.gif)
{{%/tab%}}

{{%/tabs%}}
