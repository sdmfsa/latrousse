---
title: Notation
keywords: syntaxe, python, matrice, vecteur, notation, apprendre, débuter
description: "Les bases de Python : vecteurs et matrices avec pandas"
weight: 0
---

## Vecteurs

Pour créer le vecteur $(2,3,5,7)$ sur `Python` avec le paquetage `pandas`, il suffit de créer un `dictionnary/array/numpy.ndarray` pour arranger les données. Ensuite, on utilise la fonction `pandas.Series` pour obtenir notre vecteur comme expliqué dans la documentation officielle {{%extlink "https://pandas.pydata.org/pandas-docs/stable/user_guide/dsintro.html"%}}.

{{%tabs%}}

{{%tab "Code"%}}

1. Créez une `array` avec les chiffres `{2, 3, 5, 7}`.

```python
my_array = [2, 3, 5, 7]  # Sans pandas
```

1. Sauvegardez les données en tant que `pandas.Series`.

```python
import pandas as pd
my_array = [2, 3, 5, 7]  # Sans pandas
vecteur = pd.Series(my_array)  # Avec pandas
```

1. Affichez les données sur l'écran.

```python
print(vecteur)  # Affiche le contenu de la variable vecteur
```

2. Utilisez l'attribut `.size` pour connaître la taille du vecteur:

```python
vecteur <- pd.Series([2,3,5,7])
vecteur.size # Affiche la longueur du vecteur
```

{{%/tab%}}

{{%tab "Python"%}}
![](vector_python.gif)
{{%/tab%}}


{{%/tabs%}}


## Matrices

Pour créer une matrice $M_{4 \times 2}$ sur `Python`, on va utiliser le paquetage Pandas (Numpy est aussi populaire). Pour apprendre davantage sur `pandas`, veuillez consulter la documentation officielle {{%extlink "https://pandas.pydata.org/docs/getting_started/intro_tutorials/01_table_oriented.html"%}}.

{{%tabs%}}

{{%tab "Code"%}}

1. Pour créer une matrice 4 par 2,  il suffit d'utiliser la fonction `pandas.DataFrame`.

```python
import pandas as pd
ma_matrice = [(2,3),
             (5,7),
             (2,4),
             (3,2)]
ma_matrice = pd.DataFrame(ma_matrice)
```

2. Pour élever chaque nombre de la matrice au carré, on utilise la méthode `pow` :

```python
ma_matrice.pow(2)
```

{{%/tab%}}

{{%tab "Python"%}}
![](matrix_python.gif)
{{%/tab%}}


{{%/tabs%}}

## Indexation

Pour accéder au énième élément du vecteur `mon_vecteur` créé avec Pandas, il suffit d'utiliser la syntaxe `.iloc[]`. Par exemple, la commande `mon_vecteur.iloc[0]` retourne le 1er élément du vecteur `mon_vecteur`. D'ailleurs, cette même syntaxe nous permet d'accéder à l'élément se trouvant dans la ligne $i$, colonne $j$, d'une matrice : il suffit de séparer la ligne et la colonne par une virgule. Par exemple, la commande `ma_matrice.iloc[0,2]` retourne l'élément correspondant à la ligne `1`, colonne `3`, de la matrice `ma_matrice`.

{{%notice tip%}}
Sur Python, on compte à partir de zéro. Ainsi, la première position d'un vecteur se trouve à l'indice `0` eu lieu de `1`.
{{%/notice%}}

```python
import pandas as pd

mon_vecteur = pd.Series([2,3,5,7])
mon_vecteur.iloc[3]  # Affiche 7

ma_matrice = pd.DataFrame([(2,3), (5,7), (2,4), (3,2)])
ma_matrice.iloc[3,1]  # Affiche 2
```

{{%notice note%}}
Il est aussi possible de choisir plusieurs lignes/colonnes d'une matrice : il suffit d'indiquer les positions à l'aide d'un vecteur. Par exemple, la commande `ma_matrice.iloc[0:2, 0]` retourne les premières deux lignes de la matrice (lignes = `0:2`), colonnes 1 (colonnes = `0`). Consultez la documentation officielle pour plus de détails {{%extlink "https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html"%}}.
{{%/notice%}}
