---
title: Notation
keywords: syntax, python, matrix, vector, notation, learn
description: "Python basics : vectors and matrices with Pandas"
weight: 0
---

## Vectors

To create a vector such as $(2,3,5,7)$ using `Python` and the `pandas` package, we need to first use a `dictionnary/array/numpy.ndarray` to store our data. Next, we use the `pandas.Series` function to get out vector as explained in the official documentation {{%extlink "https://pandas.pydata.org/pandas-docs/stable/user_guide/dsintro.html"%}}.

{{%tabs%}}

{{%tab "Code"%}}

1. Create an `array` using the following numbers :  `{2, 3, 5, 7}`.

```python
my_array = [2, 3, 5, 7]  # Without pandas
```

1. Save the data as a `pandas.Series`.

```python
import pandas as pd
my_array = [2, 3, 5, 7]  # Without pandas
vecteur = pd.Series(my_array)  # With pandas
```

1. Print the data on the screen.

```python
print(vecteur)  # Shows the content of the vecteur variable
```

2. Use the `.size` attribute to know the vector size.

```python
vecteur <- pd.Series([2,3,5,7])
vecteur.size # Shows the vector size
```

{{%/tab%}}

{{%tab "Python"%}}
![](vector_python.gif)
{{%/tab%}}


{{%/tabs%}}


## Matrices

To obtain an $M_{4 \times 2}$ matrix with `Python`, we will use the the Pandas package (Numpy is also popular). To learn more about `pandas`, check the official documentation {{%extlink "https://pandas.pydata.org/docs/getting_started/intro_tutorials/01_table_oriented.html"%}}.

{{%tabs%}}

{{%tab "Code"%}}

1. To create a 4 by 2 matrix, use the `pandas.DataFrame` function.

```python
import pandas as pd
ma_matrice = [(2,3),
             (5,7),
             (2,4),
             (3,2)]
ma_matrice = pd.DataFrame(ma_matrice)
```

2. Use the `pow` method to get the element-wise square of the `DataFrame`.


```python
ma_matrice.pow(2)
```

{{%/tab%}}

{{%tab "Python"%}}
![](matrix_python.gif)
{{%/tab%}}


{{%/tabs%}}

## Indexing

To access the $n$ element of a vector `mon_vecteur` created with Pandas, we will use the `.iloc[]` syntax. For example, the `mon_vecteur.iloc[0]` command would return the 1st element of the `mon_vecteur` vector. This syntax also allows access to the elements of a matrix: `ma_matrice.iloc[0,2]` returns the element located in the intersection of the first row and third column of `ma_matrice`.

{{%notice tip%}}
We count starting from zero on Python, which means that the first element of a vector is located in position `0` instead of `1`.
{{%/notice%}}

```python
import pandas as pd

mon_vecteur = pd.Series([2,3,5,7])
mon_vecteur.iloc[3]  # Prints 7

ma_matrice = pd.DataFrame([(2,3), (5,7), (2,4), (3,2)])
ma_matrice.iloc[3,1]  # Prints 2
```

{{%notice note%}}
It is also possible to choose multiple rows/columns using the same syntax. For example, the `ma_matrice.iloc[0:2, 0]` command returns the first two lines from the matrix (rows = `0:2`), column 1 (columns = `0`). Check the official documentation for more details {{%extlink "https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html"%}}.
{{%/notice%}}
