---
title: "RIT"
date: 2020
icon: "ti-bar-chart-alt"
description: Le logiciel Rotman Interactive Trader (RIT) est un simulateur de marché développé par la Rotman School of Management à l'Université de Toronto.
keywords: rit, logiciel, simulation, boursière, rotman, interactive, trader, club, simulation, ritc, international, compétition
type : "docs"
weight: 3
---

