---
title: Premiers pas
keywords: lancer, rit, connexion, login, graphique, portefeuille, trader, info, transiger
description: Un tour rapide pour se familiariser avec le simulateur de marché Rotman Interactive Trader (RIT).
weight: 0
---

Introduction aux outils disponibles sur le simulateur de marché *Rotman Interactive Trader (RIT)*.

## Connexion

{{%tabs%}}
{{%tab "Instructions"%}}

1. Lancez `RIT` et connectez-vous à l'aide de l'identifiant de connexion transmis par votre enseignant.

{{%notice note%}}
Chaque simulation requiert un nom d'utilisateur, mot de passe et port.
{{%/notice%}}

{{%/tab%}}
{{%tab "Windows"%}}

![](rit_login.gif)

{{%/tab%}}
{{%/tabs%}}

## Tour rapide

Le logiciel `RIT` vous permet de personnaliser votre aire de travail avec plusieurs outils, dont :

+ `Portfolio` vous montre les titres disponibles sur le marché et vos positions.
+ `Order Entry > Buy/Sell` vous permet d'acheter (`buy`) et de vendre (`sell`) les titres.
+ `Market Depth > Book Trader` vous montre le livre d'ordres, c'est-à-dire le marché divisé en deux colonnes : les vendeurs du côté droit (`ask`) et les acheteurs du côté gauche (`bid`).
+ `Trade Blotter` vous montre un registre des transactions complétées et en attente.
+ `Transaction Log` vous montre un registre de toutes vos transactions.
+ `Trader Info` vous donne des informations en temps réel sur votre profit et perte ainsi que sur vos limites de transaction.
+ `Charting > Security Chart` vous aide à visualiser le prix des titres disponibles.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Cliquez sur `Portfolio` et glisser-déposez la fenêtre où vous le désirez.
2. Cliquez sur `Order Entrey > Buy/Sell`. Vous pouvez aussi cliquez sur le symbole `+` si vous préférez utiliser plusieurs boutons `Buy/Sell`.
3. Cliquez sur `Market Depth > Book Trader`.
4. Cliquez sur `Trade Blotter`.
5. Cliquez sur `Transaction Log`.
6. Cliquez sur `Trader Info`.
7. Cliquez sur `Charting > Security Chart`.
8. Placez toutes les fenêtres selon vos préférences !

{{%notice tip%}}
Le guide officiel **Client Software Feature Guide** a beaucoup plus de détails sur les modules disponibles et vous devriez le lire pour comprendre en profondeur le fonctionnement du logiciel {{%extlink "https://306w.s3.amazonaws.com/rit_cases/Help/Student/RIT%20-%20User%20Guide%20-%20Client%20Software%20Feature%20Guide.pdf"%}}.
{{%/notice%}}

{{%/tab%}}
{{%tab "Windows"%}}

![](rit_tour.gif)

{{%/tab%}}
{{%/tabs%}}

## Énoncé des cas

Vous trouverez un fichier nommé `Case Brief` qui explique la simulation en détail via le module `Case Files`. Selon la simulation, vous trouverez également  des fichiers de support `Excel`.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Cliquez sur `Case Files`.
2. Cliquez sur les liens disponibles.
3. Enregistrez une copie du fichier au besoin.

{{%/tab%}}
{{%tab "Windows"%}}

![](rit_files.gif)

{{%/tab%}}
{{%/tabs%}}
