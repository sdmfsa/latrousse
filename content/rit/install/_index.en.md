---
title: Installing RIT
keywords: how,to, install, rit, rotman, interactive, trader, software, simulation, market, simulator
weight: 0
---

Rotman Interactive Trader (RIT) can be installed on **Windows**. If you work on **MAC**, it is recommended to install Windows 10 on your MAC with *Boot camp Assistant* {{%extlink "https://support.apple.com/en-ca/HT201468"%}}.

## Downloading the software

{{%tabs%}}
{{%tab "Instructions"%}}

1. Go to RIT's official website at `https://rit.rotman.utoronto.ca/` and click on `Downloads` {{%extlink "https://rit.rotman.utoronto.ca/software.asp"%}}.

2. Download and install the `Download 1 (RIT Client)` file.

3. Download and install the `Download 2 (Excel RTD Links)` file, which will allow you to access `RIT` through `Excel` or any programming language.

{{%notice info%}}
You will find two different files available and it is important to choose the one corresponding to your `Excel` version (`32-bit vs 64-bit`). To verify this, click on `File > Account > About Excel`.
{{%/notice%}}

{{%/tab%}}
{{%tab "Windows"%}}

![](rit_install.gif)

{{%/tab%}}
{{%/tabs%}}

## Testing the installation

{{%tabs%}}
{{%tab "Instructions"%}}

1. Launch `RIT` and log in using the username, password, server and port provided by your instructor. If you haven't yet received your credentials, you can can still connect to the `demo` server at University of Toronto {{%extlink "https://rit.rotman.utoronto.ca/demo.asp"%}}.

2. Once you are connected to `RIT` click on `Portfolio` and then launch `Excel`.

3. Drag and drop any field from your `Portfolio` into `Excel`, e.g. the `Last` price. The data should update on `Excel` as the simulation progresses.

{{%notice info%}}
If the `RIT` data is not updating on `Excel`, make sure that you have installed the correct version of file `Download 2 (Excel RTD Links)`. If you still experience the same issue, you need to verify that your computer satisfies the minimum system requirements as explained on the `RIT` website. A common issue is missing the `Microsoft .NET 4.0 Framework` {{%extlink "https://rit.rotman.utoronto.ca/software.asp"%}}.
{{%/notice%}}

{{%/tab%}}
{{%tab "Windows"%}}

![](rit_test.gif)

{{%/tab%}}
{{%/tabs%}}
