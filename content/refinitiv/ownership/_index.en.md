---
title: Ownership data
keywords: how, to, find, ownership, investor, data, profile, eikon, thomson, reuters, refinitiv
description: Finding ownership and investors data with Refinitiv Workspace.
weight: 1
---

To find investor data {{%extlink "https://www.refinitiv.com/content/dam/marketing/en_us/documents/fact-sheets/ownership-profiles-fact-sheet.pdf"%}} related to a security, we can use:

+ The [SHARE](#software) function on *Refinitiv Workspace*.
+ The *Data API* through a [programming language](#eikon-api).

{{%notice note %}}
To obtain historical data, we can use additional parameters via the API. These parameters as well as other available fields can be validated via the DIB function on *Refinitiv Workspace*. To learn more about the API, visit the official documentation {{%extlink "https://developers.refinitiv.com/en/api-catalog/eikon/eikon-data-api/documentation"%}}.
{{%/notice%}}

## Refinitiv Workspace {#software}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Enter the stock unique ID or `RIC` followed by `SHARE`, then hit the `<Enter>` key.

2. Customize the ownership table and export to Excel as needed. Please note that only the raw data will be exported (no formulas).

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](ownership_software.gif)

{{%/tab%}}
{{%/tabs%}}


## Eikon Data API {#eikon-api}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Identify the mnemonics of the fields to extract. You can either use the `DIB` function or place the cursor over the `?` symbol of each column in the `SHARE` table.

2. Make your API call with the `get_data` function using the `RIC` and mnemonics.

3. Use additional parameters as needed, for instance the `SDate` parameter allows you to choose a specific date.

{{%/tab%}}
{{%tab "R"%}}

```r
eikonapir::set_app_id(Sys.getenv("eikon_data_api_id"))
mydata <- eikonapir::get_data("BB", "TR.InvestorFullName(SDate=2016-01-01);TR.PctOfSharesOutHeld(SDate=2016-01-01)")
head(mydata)
```

{{%notice tip %}}
To get the date corresponding to a data point instead of its value, add `.date` at the end of the mnemonic. You can validate other parameters via the `DIB` function on *Refinitiv Workspace*.
{{%/notice%}}


![](ownership_api.gif)

{{%/tab%}}
{{%/tabs%}}



