---
title: Profil des Investisseurs
keywords: comment, trouver, investisseurs, profil, eikon, thomson, reuters, refinitiv
description: Comment trouver des informations sur les investisseurs avec Refinitiv Workspace.
weight: 1
---

Pour trouver des données sur les investisseurs {{%extlink "https://www.refinitiv.com/content/dam/marketing/en_us/documents/fact-sheets/ownership-profiles-fact-sheet.pdf"%}} en relation avec un titre, on peut utiliser :

+ La fonction [SHARE](#software) sur *Refinitiv Workspace*.
+ Le *Data API* avec un [langage de programmation](#eikon-api).

{{%notice note %}}
Pour obtenir des données historiques, il suffit d'utiliser des paramètres additionnels via l'API. Ces paramètres ainsi que les champs disponibles peuvent être validés via la fonction `DIB` sur *Refinitiv Workspace*. Pour apprendre davantage sur l'API, visitez la documentation officielle {{%extlink "https://developers.refinitiv.com/en/api-catalog/eikon/eikon-data-api/documentation"%}}.
{{%/notice%}}

## Refinitiv Workspace {#software}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Rentrez le `RIC` suivi du mot `SHARE`, appuyez sur la touche `<Entrée>`

2. Personnalisez le tableau et exportez les données vers Excel au besoin. Notez que seul les données brutes (sans formules) seront téléchargées.

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](ownership_software.gif)

{{%/tab%}}
{{%/tabs%}}


## Eikon Data API {#eikon-api}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Identifiez les mnémoniques des champs à extraire. Vous pouvez soit utiliser la fonction `DIB`, soit placer le curseur sur le symbole `?` de chaque colonne du tableau `SHARE`.

2. Faites votre appel avec la fonction `get_data` à l'aide du `RIC` et des mnémoniques.

3. Utilisez des paramètres additionnels au besoin, par exemple le paramètre `SDate` vous permet d'indiquer une date précise.


{{%/tab%}}
{{%tab "R"%}}

```r
eikonapir::set_app_id(Sys.getenv("eikon_data_api_id"))
mydata <- eikonapir::get_data("BB", "TR.InvestorFullName(SDate=2016-01-01);TR.PctOfSharesOutHeld(SDate=2016-01-01)")
head(mydata)
```

{{%notice tip %}}
Pour obtenir la date correspondant à une donnée au lieu de sa valeur, ajoutez `.date` à la fin du mnémonique. Vous pouvez valider d'autres paramètres via la fonction `DIB` sur *Refinitiv Workspace*
{{%/notice%}}


![](ownership_api.gif)

{{%/tab%}}
{{%/tabs%}}



