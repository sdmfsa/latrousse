---
title: ESG data
keywords: how, to, find, esg, data, environment, social, governance, eikon, thomson, reuters, refinitiv
description: Finding and downloading ESG (environment, social, governance) data with Refinitiv Workspace.
weight: 1
---

To find ESG (environment, social, governance) data {{%extlink "https://www.refinitiv.com/content/dam/marketing/en_us/documents/methodology/refinitiv-esg-scores-methodology.pdf"%}} on a financial security, we can use the `ESG` function.

## Software {#software}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Enter a `RIC` followed by `ESG`, then hit the `<Enter>` key.

2. Customize the table and export the data to Excel as needed. Note that it is possible to export either the raw data (without formulas) or the file with formulas.

{{%notice tip%}}
To get historical data, change the settings on the left side of the `ESG` screen.
{{%/notice%}}

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](esg_software.gif)

{{%/tab%}}
{{%/tabs%}}


