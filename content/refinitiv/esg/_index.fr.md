---
title: Données ESG
keywords: comment, trouver, données, esg, environnement, social, gouvernance, eikon, thomson, reuters, refinitiv
description: Comment trouver des données ESG (environnement, social, gouvernance) avec Refinitiv Workspace.
weight: 1
---

Pour trouver des données ESG (environnement, social, gouvernance) {{%extlink "https://www.refinitiv.com/content/dam/marketing/en_us/documents/methodology/refinitiv-esg-scores-methodology.pdf"%}} concernant un titre financier, il suffit d'utiliser la fonction ESG.

## Un titre {#software}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Rentrez le `RIC` suivi du mot `ESG`, appuyez sur la touche `<Entrée>`.

2. Personnalisez le tableau et exportez les données vers Excel au besoin. Notez qu'il est possible d'exporter soit les données brutes (sans formules), soit le fichier avec des formules.

{{%notice tip%}}
Pour obtenir des données historiques, modifiez les paramètres à gauche de l'écran `ESG`.
{{%/notice%}}

{{%/tab%}}

{{%tab "Refinitiv"%}}

![](esg_software.gif)

{{%/tab%}}
{{%/tabs%}}

## Plusieurs titres

> Dans cet exemple nous créons une liste de titres basés sur l'indice `S&P/TSX Composite` pour ensuite extraire les scores ESG correspondants.

{{%tabs%}}
{{%tab "Instructions"%}}

1.   Accédez à la page de l'indice en question. Dans ce cas-ci, nous tapons `TSX` dans la barre de recherche pour retrouver la page d'accueil du S&P/TSX Composite Index.
2.   Accédez à la liste des constituents de l'indice sous l'onglet  `Constituents`.
3.   Sous l'onglet `SUMMARY REPORT`, cliquez sur `Create List` pour isoler la liste de titres.
4.  Sélectionnez les titres voulus dans le menu déroulant ou tous les titres au moyen du bouton `Select All`.
5.   Cliquez sur le bouton `Open in Quote List` pour ouvrir un nouveau moniteur avec la liste de titres.
6.   Double-cliquez sur le nom d'une colonne pour changer le contenu ou ouvrir une nouvelle colonne avec le bouton `+`.
7.   Tapez `ESG SCORE` et cliquez sur 'ESG Score' dans le menu déroulant qui apparaît.

{{%/tab%}}

{{%tab "Refinitiv"%}}

![](esg_list.gif)

{{%/tab%}}
{{%/tabs%}}

### Données Excel

{{%tabs%}}
{{%tab "Instructions"%}}

1.   En maintenant la touche `CTRL` enfoncée, sélectionnez les colonnes à isoler.
2.   Appuyez sur les touches `CTRL` + `C` pour copier les colonnes dans Refinitiv.
3.   Appuyez sur  `CTRL` + `V` pour coller les colonnes dans Excel.

{{%/tab%}}

{{%tab "Refinitiv"%}}

![](esg_excel.gif)

{{%/tab%}}
{{%/tabs%}}

## Avec Python

> Commencez par suivre les instructions d'installation avec Python [ici](https://sdmapps.ca/latrousse/fr/refinitiv/install/).


{{%tabs%}}
{{%tab "Instructions"%}}

1. Trouvez la liste de constituants de l'indice.

```python
import eikon as ek
ek.set_app_key('DEFAULT_CODE_BOOK_APP_KEY')

tsx_df, err = ek.get_data(
    instruments=".GSPTSE",
    fields=["TR.IndexConstituentRIC","TR.IndexConstituentName"])
display(tsx_df)
```

2. Faites une requête pour extraire les scores ESG avec le champ `TR.TRESGScore`.

```python
tsx_ticker_list = tsx_df['Constituent RIC'].to_list()

tsx_esg_scores_df, err = ek.get_data(
    instruments = tsx_ticker_list,
    fields = ['TR.TRESGScore'])
display(tsx_esg_scores_df.sort_values(by='ESG Score', ascending=False))
```

{{%/tab%}}

{{%tab "Python"%}}

![](esg_python.gif)

{{%/tab%}}
{{%/tabs%}}



