---
title: Identifiant unique
keywords: comment, trouver, identifiant, unique, eikon, thomson, reuters, ric, ticker, refinitiv, permid, mnemonique
description: Comment trouver des identifiants uniques comme un RIC ou un permID avec Refinitiv Workspace.
weight: 0
---

Il est possible de trouver l'identifiant unique d'un titre financier, par exemple un [RIC](#ric-note), à l'aide d'une des méthodes suivantes :

+ La boîte de recherche du [logiciel](#software).
+ Le complément [Excel](#excel-addin).
+ Le service [PermID](#permid-org)

{{%notice note ric-note%}}
Le *Reuters Instrument Code* (RIC) et le *Refinitiv Permanent Identifier* (PermID) sont des identifiants uniques utilisés par la base de données `Refinitiv Eikon` . Il **ne s'agit pas** du même ticker utilisé par `Bloomberg` ou toute autre base de données.
{{%/notice%}}

Les instructions suivantes vous permettront de chercher l'identifiant unique d'une compagnie à l'aide de mots-clés tels que le nom de la compagnie.

## Refinitiv Workspace {#software}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Rentrez les mots clés du nom de la compagnie recherchée dans la boîte de recherche de la barre d'outils, soit en haut de l'écran.

{{%notice tip%}}
Portez attention aux lettres affichées du côté gauche des résultats. Par exemple, `EQ` signifie `Equity` (actions) et `FUT` signifie `Futures contract` (contrats à terme), tandis que `APP` signifie `Apps & Tools` (Applications et outils).
{{%/notice%}}

2. Choisissez le titre financier souhaité, par exemple `BB.TO`, soit en cliquant dessus, soit en appuyant sur la touche `<Entrée>`.

3. Vous pouvez copier le `RIC` en faisant un clic droit pour choisir `Copy`.

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](ric_software.gif)

{{%/tab%}}
{{%/tabs%}}


## Complément Excel {#excel-addin}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Cliquez sur le bouton `Build Formula`.

2. Choisissez le type d'actif sous le champ `Instruments`, par exemple `Stocks` pour actions.

3. Rentrez les mots clés pour votre recherche, par exemple le nom d'une entreprise.

4. Choisissez le titre financier souhaité en cliquant dessus, par exemple `BB.TO` .

5. Complétez votre requête pour obtenir d'autres identifiants uniques, rentrez par exemple le code [(mnémonique)](#data-item-browser) `TR.RIC` pour le RIC et `TR.ISIN` pour l'ISIN.


{{%/tab%}}
{{%tab "Excel"%}}

![](ric_excel.gif)

{{%/tab%}}

{{%/tabs%}}

### Codes (mnémoniques) {#data-item-browser}

L'outil `Data Item Browser` disponible via *Refinitiv Workspace* nous permet de confirmer la syntaxe correspondant à un champ de données.  Il s'agit de l'équivalent de la fonction `FLDS` sur Bloomberg.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Lancez l'outil `DIB`.
2. Cherchez par mot clé une donnée quelconque, par exemple `Revenu`.
3. Le mnémonique se trouve sous la colonne `Data Item Code`, par exemple `TR.EVToSales`.

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](dib.gif)

{{%/tab%}}
{{%/tabs%}}

## permid.org {#permid-org}

Ce service vous permet d'obtenir un grand nombre d'identifiants uniques avec une seule recherche à l'aide d'un gabarit `CSV`. Il suffit de téléverser le gabarit rempli avec le plus d'information possible.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Allez sur le site `https://permid.org/match`.
2. Téléchargez le gabarit `CSV` correspondant à votre recherche, par exemple *Organization* si vous cherchez l'identifiant unique d'une entreprise.
3. Remplissez le gabarit avec les informations de votre recherche.
4. Téléversez le gabarit `CSV` rempli sur le site web.

{{%/tab%}}


{{%tab "Refinitiv"%}}
![](permid_org.gif)
{{%/tab%}}

{{%/tabs%}}

### Excel

Le PermID peut être utilisé directement dans une requête via `Excel`.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Rentrez le `permid` comme premier argument d'une formule `RDP.Data`. Par exemple, `4295861766` au lieu de `"BB.TO"` comme dans les formules suivantes :

```r
=RDP.Data(4295861766,"TR.RIC;TR.ISIN")
=RDP.Data(4295861766,"TR.PriceClose","SDate=20201001 EDate=20201101 Frq=D CH=Fd RH=IN")
```

{{%/tab%}}

{{%tab "Excel"%}}
![](permid_use.gif)
{{%/tab%}}

{{%/tabs%}}


### REST API

Le service `permid.org/match` peut aussi être utilisé via une REST API. Pour ce faire, vous devez vous enregistrer en ligne pour obtenir votre clé d'accès ou *token* <a href="https://developers.refinitiv.com/en/api-catalog/open-perm-id/permid-record-matching-restful-api" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.


{{%tabs%}}
{{%tab "Instructions"%}}

Cet exemple `R` utilise les paquetages `httr` et `jsonlite` disponibles sur `CRAN`. Sur `Python`, le paquetage `requests` disponible via `pip` vous aide avec les requêtes `http`.

1. Sauvegardez votre *PermID token* dans le fichier `.Renviron`. Vous devriez y ajouter une ligne comme celle-ci :

```r
perm_id_token = "324FGASadsfaseiuo7362y401q372"
```

{{%notice tip%}}
Le fichier `.Renviron` se trouve dans le dossier `~/`, soit `Documents` sur Windows et `/Users/votreutilisateur` sur `MAC`.
{{%/notice%}}

2. Envoyez la requête `http` suivante. Cet exemple utilise le paquetage `httr` pour envoyer une requête `GET` et le paquetage `jsonlite` pour convertir la réponse `json` en `data.frame`.

```r
library(httr)
library(jsonlite)

r <- httr::GET("https://api-eit.refinitiv.com/permid/search",
              query = list(q = company_name, entityType = "organization", num = 10),
              add_headers("X-AG-Access-Token" = Sys.getenv("perm_id_token")))

json_response <- content(r, as = "text")
new_response <- jsonlite::fromJSON(json_response)

head(new_response$result$organizations$entities)
```

> Vous obtiendrez les 10 meilleurs résultats disponible via `permid.org/match` sous `new_response\$result\$organizations\$entities`.

{{%/tab%}}

{{%tab "R"%}}
![](permid_api.gif)
{{%/tab%}}

{{%/tabs%}}

{{%notice tip%}}
Tous les détails sur l'utilisation de l'API PermID se trouvent en ligne <a href="https://developers.refinitiv.com/en/api-catalog/open-perm-id/permid-record-matching-restful-api" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.
{{%/notice%}}

