---
title: Unique identifier
keywords: how, to, find, eikon, thomson, reuters, ric, ticker, identifier, id, unique, refinitiv, workspace, permid
description: How to find unique identifiers such as a PermID or RIC using Refinitiv workspace.
weight: 0
---

We can find financial security's unique identifiers such as a [RIC](#ric-note) using the `Eikon` with the following methods:

+ Using the [search](#software) box.
+ Using the [Excel](#excel-addin) add-in.
+ The [PermID](#permid-org) service.

{{%notice note ric-note%}}
The Reuters Instrument Code (`RIC`) and *Refinitiv Permanent Identifier* (PermID) are unique identifiers used by the `Refinitiv Eikon` database. This is **not the same** as the `Bloomberg ticker` nor any other ticker.
{{%/notice%}}

The following instructions allow you to manually search for a company's unique identifier (ticker) using keywords such as the company name.

## Refinitiv Workspace {#software}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Start by typing the name of the company you are looking for in the *Search* box on the top of the screen.

{{%notice tip%}}
Pay attention to the letters on the left side of the autocomplete results. For instance, `EQ` means `Equity` and `FUT` means `Futures contract`, while `APP` means `Apps & Tools`.
{{%/notice%}}

2. Choose the security you are interested in, e.g. `BB.TO`, and either click on it or hit the `<Enter>` key.

3. You can copy the `RIC` by right clicking on it and choosing `Copy`.

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](ric_software.gif)

{{%/tab%}}
{{%/tabs%}}

## Excel Add-in {#excel-addin}

{{%tabs%}}
{{%tab "Instructions"%}}

1. Click on `Build Formula`.

2. Choose the asset class on the `Instruments` menu, e.g. `Stocks`.

3. Enter some keywords, for example a company name.

4. Choose a financial security, e.g. `BB.TO` .

5. Complete your query with additional unique identifiers ([mnemonics](#data-item-browser)) such as `TR.RIC` for the RIC and `TR.ISIN` for the ISIN.

{{%/tab%}}
{{%tab "Excel"%}}

![](ric_excel.gif)

{{%/tab%}}
{{%/tabs%}}


### Codes (mnemonics) {#data-item-browser}

The `Data Item Browser` APP on  *Refinitiv Workspace* helps us when looking for a data item code.  This is equivalent to the `FLDS` function on Bloomberg.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Launch `DIB`.
2. Enter some keywords for your search, e.g. `Revenu`.
3. The mnemonic is on the `Data Item Code` column, e.g. `TR.EVToSales`.

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](dib.gif)

{{%/tab%}}
{{%/tabs%}}

## permid.org {#permid-org}

This service helps you obtain a large number of unique identifiers on a single search using a `CSV` template. Fill out the template with as many details as possible and upload it to get your results.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Go to `https://permid.org/match`.
2. Download the desired `CSV` template , for example the *Organization* template if you need to find a company's unique id.
3. Fill out the template with your own search terms.
4. Upload the `CSV` template.

{{%/tab%}}


{{%tab "Refinitiv"%}}
![](permid_org.gif)
{{%/tab%}}

{{%/tabs%}}

### Excel

The PermID can be used directly on an `Excel` query.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Enter the `permid` as first parameter on a `RDP.Data` formula. For example, use `4295861766` instead of `"BB.TO"` as in the following formulas :

```r
=RDP.Data(4295861766,"TR.RIC;TR.ISIN")
=RDP.Data(4295861766,"TR.PriceClose","SDate=20201001 EDate=20201101 Frq=D CH=Fd RH=IN")
```

{{%/tab%}}

{{%tab "Excel"%}}
![](permid_use.gif)
{{%/tab%}}

{{%/tabs%}}


### REST API

The `permid.org/match` service is also available as a REST API. You first need to register online <a href="https://developers.refinitiv.com/en/api-catalog/open-perm-id/permid-record-matching-restful-api" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a> to get your own access key or *token*.

{{%tabs%}}
{{%tab "Instructions"%}}

This `R` example uses the `httr` and `jsonlite` packages available on `CRAN`. On `Python`, you might want to use the `requests` module through `pip`.

1. Save your *PermID token* in your `.Renviron` file. You should add a new line that looks s like the following :

```r
perm_id_token = "324FGASadsfaseiuo7362y401q372"
```

{{%notice tip%}}
The `.Renviron` file is located at `~/`, which defaults to `Documents` on Windows and to `/Users/yourusername` on `MAC`.
{{%/notice%}}

2. Send the following `http` request. This examples uses `httr` to send a `GET` request and `jsonlite` to convert the `json` response to a `data.frame`.

```r
library(httr)
library(jsonlite)

r <- httr::GET("https://api-eit.refinitiv.com/permid/search",
              query = list(q = company_name, entityType = "organization", num = 10),
              add_headers("X-AG-Access-Token" = Sys.getenv("perm_id_token")))

json_response <- content(r, as = "text")
new_response <- jsonlite::fromJSON(json_response)

head(new_response$result$organizations$entities)
```

> You will find the top 10 results available through `permid.org/match` on the `new_response\$result\$organizations\$entities` variable.

{{%/tab%}}

{{%tab "R"%}}
![](permid_api.gif)
{{%/tab%}}

{{%/tabs%}}

{{%notice tip%}}
To learn more about the PermID API, please check out the official documentation <a href="https://developers.refinitiv.com/en/api-catalog/open-perm-id/permid-record-matching-restful-api" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.
{{%/notice%}}

