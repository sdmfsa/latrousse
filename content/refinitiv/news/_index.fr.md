---
title: Nouvelles
keywords: comment, trouver, nouvelles, eikon, thomson, reuters, ticker, refinitiv
description: Comment trouver des nouvelles sur un titre avec Refinitiv Workspace.
weight: 0
---

Suivez ces instructions pour trouver des nouvelles concernant un titre quelconque.

## NEWS

{{%tabs%}}
{{%tab "Instructions"%}}

1. Allez sur l'application `NEWS`.
2. Modifiez les dates pour la recherche.
3. Cliquez sur la nouvelle à consulter.

{{%/tab%}}
{{%tab "Refinitiv"%}}

![](news.gif)

{{%/tab%}}
{{%/tabs%}}


