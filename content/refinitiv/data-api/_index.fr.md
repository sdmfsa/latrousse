---
title: Eikon Data API
keywords: comment, utiliser, eikon, data , api, eikonapir, screen, permid, ric, isin
description: Comment faire une requête avec l'Eikon Data API (Refinitiv Workspace).
weight: 0
---

L'Eikon Data API nous permet d'accéder aux données disponibles sur *Refinitiv Workspace* à l'aide d'une REST API.

{{%notice note%}}
Une API officielle pour Python est disponible via pip, tandis qu'un paquetage R est disponible sur Github {{%extlink "https://github.com/ahmedmohamedali/eikonapir"%}}.
{{%/notice%}}

## Préalables

+ Vous devez créer une clé d'accès ou *token* au préalable en suivant [ces instructions](https://developers.refinitiv.com/en/api-catalog/eikon/eikon-data-api/quick-start). Si un compte vous êtes demandé, rentrez l'adresse `@ulaval.ca` utilisée pour vous connecter au logiciel *Refinitiv  Workspace*.

+ Ces exemples utilisent le paquetage `eikonapir` disponible en ligne <a href="https://github.com/ahmedmohamedali/eikonapir" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a> pour travailler sur `R` :

```r
remotes::install_github("ahmedmohamedali/eikonapir")
```

+ Sur `Python`, il est recommandé d'utiliser le paquetage `eikon` disponible via `pip`.

```bash
pip install eikon
```

{{%notice tip%}}
Plusieurs tutoriels pour `Python` sont disponibles sur le site de Refinitiv <a href="https://developers.refinitiv.com/en/api-catalog/eikon/eikon-data-api/tutorials#symbology" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.
{{%/notice%}}


## Identifiant unique

À l'aide de la fonction `SCREEN`, nous pouvons appliquer des filtres à toute compagnie qui se trouve dans la base de données, dont des **compagnies privées**.

La syntaxe de la fonction est la suivante :

```r
SCREEN('Universe', 'Filtres')
```

Et elle doit être utilisée comme premier argument dans une requête vers l'API avec la méthode `get_data`, comme dans l'exemple `R` suivant.


{{%tabs%}}
{{%tab "Instructions"%}}

### R

1. Sauvegardez votre *Eikon Data API ID* dans le fichier `.Renviron`. Vous devriez y ajouter une ligne comme celle-ci :

```r
eikon_data_api_id = "324FGASadsfaseiuo7362y401q372"
```

2. Exécutez le code suivant pour faire un *screening* avec le mot clé `blackberry` :

```r
library(eikonapir)
set_app_id(Sys.getenv("eikon_data_api_id")) # Set your key on .Renviron

company_name <- "blackberry"
fields <- "TR.OrganizationID;TR.RIC;TR.ISIN;TR.CommonName;TR.IsPublic;TR.CompanyPublicSinceDate"
screen_str <- sprintf("SCREEN(U(IN(Equity(public,private,primary))),Contains(TR.CommonName,'%s'))", company_name)

head(eikonapir::get_data(screen_str, fields))
```

### Python


1. Sauvegardez votre *Eikon Data API ID* dans le fichier `eikon.cfg`. Vous devriez y ajouter une ligne comme celle-ci :

```python
[eikon]
app_id = 324FGASadsfaseiuo7362y401q372
```

2. Exécutez le code suivant pour faire un *screening* avec le mot clé `blackberry` :

```python
import eikon as ek
import configparser as cp

cfg = cp.ConfigParser()
cfg.read('eikon.cfg')  # Set your key on eikon.cfg

ek.set_app_key(cfg['eikon']['app_id'])

company_name = "blackberry"
fields = ["TR.OrganizationID", "TR.RIC", "TR.ISIN", "TR.CommonName", "TR.IsPublic", "TR.CompanyPublicSinceDate"]
screen_str = "SCREEN(U(IN(Equity(public,private,primary))),Contains(TR.CommonName,'" + company_name + "'))"

data_grid, err = ek.get_data([screen_str], fields)
data_grid.head()
```

{{%/tab%}}

{{%tab "R"%}}

![](api_r.gif)

{{%/tab%}}

{{%tab "Python"%}}

![](api_python.gif)

{{%/tab%}}

{{%/tabs%}}


La requête ci-haut retournera les champs d'extraction de la variable `fields`, dont le `TR.RIC`, pour tout instrument financier satisfaisant les filtres appliqués avec la fonction `SCREEN`. Les champs d'extraction disponibles peuvent être obtenus via *Refinitiv Workspace* avec le `Data item Browser` (DIB).

## Conversion de RIC

La fonction `get_symbology` retourne un identifiant unique cible à partir d'un identifiant unique source. Par exemple, nous pouvons obtenir un `ISIN` ou un `SEDOL` à partir d'un `RIC`.

{{%tabs%}}
{{%tab "Instructions"%}}

### R

1. Exécutez le code suivant pour obtenir plusieurs identifiants uniques à partir de `BB.TO` :

```r
ric_id <- "BB.TO"
isin_id <- "CA09228F1036"

get_symbology(isin_id, from_symbol_type = "ISIN", to_symbol_type = "RIC")  # BB.TO
get_symbology(ric_id, from_symbol_type = "RIC", to_symbol_type = "ISIN")
get_symbology(ric_id, from_symbol_type = "RIC", to_symbol_type = "SEDOL")
```

### Python


1. Exécutez le code suivant pour obtenir plusieurs identifiants uniques à partir de `BB.TO` :

```python
ric_id = "BB.TO"
isin_id = "CA09228F1036"

ek.get_symbology([isin_id], from_symbol_type = "ISIN", to_symbol_type = "RIC")  # BB.TO
ek.get_symbology([ric_id], from_symbol_type = "RIC", to_symbol_type = "ISIN")
ek.get_symbology([ric_id], from_symbol_type = "RIC", to_symbol_type = "SEDOL")
```

{{%/tab%}}
{{%tab "R"%}}

![](symbology_r.gif)

{{%/tab%}}


{{%tab "Python"%}}

![](symbology_python.gif)

{{%/tab%}}

{{%/tabs%}}

## Prix historiques


La fonction `get_timeseries` retourne une série de données chronologiques.

{{%tabs%}}
{{%tab "Instructions"%}}

### R

1. Sauvegardez les RICs dans une variable nommée `rics`. Sauvegardez les champs d'extraction dans la variable `query_fields`.

```r
rics <- list("BB.TO","AC.TO")
query_fields <- list("OPEN", "CLOSE", "HIGH", "LOW")
```

2. Exécutez le code suivant pour obtenir les données journalières indiqués dans la variable `query_fields`, entre les dates `start_date` et `end_date`.

```r
mydata <- eikonapir::get_timeseries(
  rics, fields = query_fields,
  start_date = "2020-01-01T00:00:00",
  end_date = "2020-11-01T00:00:00", interval = "daily"
)

head(mydata)
```

Les données se trouvent maintenant en forme de `data.frame` dans la variable `mydata`.


### Python

1. Sauvegardez les RICs dans une variable nommée `rics`. Sauvegardez les champs d'extraction dans la variable `query_fields`.

```python
rics = ["BB.TO","AC.TO"]
query_fields = ["OPEN", "CLOSE", "HIGH", "LOW"]
```

2. Exécutez le code suivant pour obtenir les données journalières indiqués dans la variable `query_fields`, entre les dates `start_date` et `end_date`.

```python
mydata = ek.get_timeseries(rics, fields = query_fields,
                         start_date = '2020-01-01',
                         end_date='2020-11-01', interval = 'daily')
mydata.head()
```

Les données se trouvent maintenant en forme de `data.frame Pandas` dans la variable `mydata`.

{{%/tab%}}
{{%tab "R"%}}

![](historical_r.gif)

{{%/tab%}}

{{%tab "Python"%}}

![](historical_py.gif)

{{%/tab%}}

{{%/tabs%}}
