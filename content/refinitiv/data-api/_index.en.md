---
title: Eikon Data API
keywords: eikon, data, api, ric, ticker, identifier, id, unique, refinitiv, workspace, eikonapir, isin, sedol, permid
description: Finding unique identifiers and historical prices with the Eikon Data API (Refinitive workspace).
weight: 0
---

The Eikon Data API helps us access the data available on Refinitiv Workspace through a REST API.

{{%notice note%}}
An API is available for Python via pip while an R package is available through Github {{%extlink "https://github.com/ahmedmohamedali/eikonapir"%}}.
{{%/notice%}}

## Before you begin

+ You first need to create an access key or *token* by following [this](https://developers.refinitiv.com/en/api-catalog/eikon/eikon-data-api/quick-start) instructions. If you are asked to create an account, you can enter the same `@ulaval.ca` account used to access *Refinitiv Workspace*.


+ The following examples use the `eikonapir` package available online <a href="https://github.com/ahmedmohamedali/eikonapir" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a> for `R` :

```r
remotes::install_github("ahmedmohamedali/eikonapir")
```

+ On `Python`, you can use the `eikon` package available through `pip`.

```bash
pip install eikon
```

{{%notice tip%}}
Several tutorials for `Python` are available on Refinitiv's website <a href="https://developers.refinitiv.com/en/api-catalog/eikon/eikon-data-api/tutorials#symbology" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.
{{%/notice%}}

## Unique identifier

Using the `SCREEN` function we can apply some filters to the entire database, include **private companies**.

The syntax looks like this :

```r
SCREEN('Universe', 'Filtres')
```

And must be used as first argument of an API call through the `get_data` method, like in the following `R` example :

{{%tabs%}}
{{%tab "Instructions"%}}

### R

1. Add your *Eikon Data API ID* to your `.Renviron` file. You should add a new line that looks like this :

```r
eikon_data_api_id = "324FGASadsfaseiuo7362y401q372"
```

2. Run the following code to perform a *screening* using `blackberry` as only input:

```r
library(eikonapir)
set_app_id(Sys.getenv("eikon_data_api_id")) # Set your key on .Renviron

company_name <- "blackberry"
fields <- "TR.OrganizationID;TR.RIC;TR.ISIN;TR.CommonName;TR.IsPublic;TR.CompanyPublicSinceDate"
screen_str <- sprintf("SCREEN(U(IN(Equity(public,private,primary))),Contains(TR.CommonName,'%s'))", company_name)

head(eikonapir::get_data(screen_str, fields))
```

### Python


1. Save your *Eikon Data API ID* on the `eikon.cfg` file. You should add a line with your *token*:

```python
[eikon]
app_id = 324FGASadsfaseiuo7362y401q372
```

2. Run the following code to perform a *screening* using `blackberry` as only input:

```python
import eikon as ek
import configparser as cp

cfg = cp.ConfigParser()
cfg.read('eikon.cfg')  # Set your key on eikon.cfg

ek.set_app_key(cfg['eikon']['app_id'])

company_name = "blackberry"
fields = ["TR.OrganizationID", "TR.RIC", "TR.ISIN", "TR.CommonName", "TR.IsPublic", "TR.CompanyPublicSinceDate"]
screen_str = "SCREEN(U(IN(Equity(public,private,primary))),Contains(TR.CommonName,'" + company_name + "'))"

data_grid, err = ek.get_data([screen_str], fields)
data_grid.head()
```

{{%/tab%}}
{{%tab "R"%}}

![](api_r.gif)

{{%/tab%}}


{{%tab "Python"%}}

![](api_python.gif)

{{%/tab%}}
{{%/tabs%}}


The  last query will return the data fields on variable `fields` for any security which passed the filters applied through the `SCREEN` function. Additional data fields are available via *Refinitiv Workspace* and the `Data item Browser` (DIB) APP.

## RIC conversion


{{%tabs%}}
{{%tab "Instructions"%}}

### R

1. Run the following code to obtain a series of unique identifiers for the `BB.TO` RIC:

```r
ric_id <- "BB.TO"
isin_id <- "CA09228F1036"

get_symbology(isin_id, from_symbol_type = "ISIN", to_symbol_type = "RIC")  # BB.TO
get_symbology(ric_id, from_symbol_type = "RIC", to_symbol_type = "ISIN")
get_symbology(ric_id, from_symbol_type = "RIC", to_symbol_type = "SEDOL")
```

### Python


1. Run the following code to obtain a series of unique identifiers for the `BB.TO` RIC:

```python
ric_id = "BB.TO"
isin_id = "CA09228F1036"

ek.get_symbology([isin_id], from_symbol_type = "ISIN", to_symbol_type = "RIC")  # BB.TO
ek.get_symbology([ric_id], from_symbol_type = "RIC", to_symbol_type = "ISIN")
ek.get_symbology([ric_id], from_symbol_type = "RIC", to_symbol_type = "SEDOL")
```

{{%/tab%}}
{{%tab "R"%}}

![](symbology_r.gif)

{{%/tab%}}


{{%tab "Python"%}}

![](symbology_python.gif)

{{%/tab%}}

{{%/tabs%}}

## Historical Prices


{{%tabs%}}
{{%tab "Instructions"%}}

### R

1. Save the RICs on the `rics` variable and the data items in the `query_fields` variable.

```r
rics <- list("BB.TO","AC.TO")
query_fields <- list("OPEN", "CLOSE", "HIGH", "LOW")
```

2. Run the following code to get the daily data indicated in the `query_fields` variable, between `start_date` and `end_date`.

```r
mydata <- eikonapir::get_timeseries(
  rics, fields = query_fields,
  start_date = "2020-01-01T00:00:00",
  end_date = "2020-11-01T00:00:00", interval = "daily"
)

head(mydata)
```

The data is now saved as a `data.frame` on the `mydata` variable.

### Python

1. Save the RICs on the `rics` variable and the data items in the `query_fields` variable.

```python
rics = ["BB.TO","AC.TO"]
query_fields = ["OPEN", "CLOSE", "HIGH", "LOW"]
```

2. Run the following code to get the daily data indicated in the `query_fields` variable, between `start_date` and `end_date`.

```python
mydata = ek.get_timeseries(rics, fields = query_fields,
                         start_date = '2020-01-01',
                         end_date='2020-11-01', interval = 'daily')
mydata.head()
```

The data is now saved as a `Pandas data.frame` on the `mydata` variable.

{{%/tab%}}
{{%tab "R"%}}

![](historical_r.gif)

{{%/tab%}}

{{%tab "Python"%}}

![](historical_py.gif)

{{%/tab%}}

{{%/tabs%}}
