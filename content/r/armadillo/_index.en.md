---
title: RcppArmadillo
keywords: how, install, rcpparmadillo, rstudio, terminal, clang, g++, armadillo, c++, cpp, r
description: Installing and using the C++ Armadillo library
weight: 0
---

The following examples show how to use the C++ Armadillo library  {{%extlink "http://arma.sourceforge.net/"%}} from and outside of `RStudio`.

{{%notice note%}}
Armadillo is a C++ library for linear algebra with a syntax similar to `R` or `Matlab`.
{{%/notice%}}


## With RStudio

> An R project, using C++.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Install the `RcppArmadillo` package {{%extlink "https://cran.r-project.org/web/packages/RcppArmadillo/index.html"%}}.
2. Click on `Create a new project`.
3. Click on `New directory > R package using RcppArmadillo`.
4. Use the `RStudio` shortcuts to compile your project {{%extlink "https://r-pkgs.org/workflows101.html"%}}.

{{%/tab%}}

{{%tab "RStudio"%}}
![](armadillo_rstudio.gif)
{{%/tab%}}


{{%/tabs%}}

{{%notice tip%}}
To learn more about the RStudio IDE and relevant shortcuts, please check the official documentation {{%extlink "https://github.com/rstudio/cheatsheets/raw/master/rstudio-ide.pdf"%}}. To learn more about package development, check out *R Packages* {{%extlink "https://r-pkgs.org/index.html"%}}.
{{%/notice%}}

## Without Rstudio

> A C++ project, using Armadillo.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Install the library on your operating system following the official documentation {{%extlink "http://arma.sourceforge.net/download.html"%}}.
2. Test your system setup with the `hello world` example available on the official documentation {{%extlink "http://arma.sourceforge.net/docs.html#example_prog"%}}.

{{%/tab%}}

{{%tab "Terminal"%}}
![](armadillo_cpp.gif)
{{%/tab%}}

{{%/tabs%}}
