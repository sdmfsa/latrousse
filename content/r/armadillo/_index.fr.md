---
title: RcppArmadillo
keywords: comment, installer, rcpparmadillo, rstudio, terminal, clang, g++, armadillo, c++, cpp, r
description: Installation et utilisation de la librairie C++ Armadillo
weight: 0
---

Les exemples suivants vous montrent comment utiliser la librairie C++ Armadillo {{%extlink "http://arma.sourceforge.net/"%}} à partir de RStudio ainsi qu'à l'extérieur de RStudio.

{{%notice note%}}
C++ Armadillo est une librairie pour faciliter les calculs matriciels dont la syntaxe ressemble celle de `R` ou `Matlab`.
{{%/notice%}}


## Avec RStudio

> Un projet R, qui utilise C++.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Installez le paquetage `RcppArmadillo` {{%extlink "https://cran.r-project.org/web/packages/RcppArmadillo/index.html"%}}.
2. Cliquez sur `Créer un nouveau projet`.
3. Cliquez sur  `Nouveau Dossier > Paquetage RcppArmadillo`.
4. Utilisez les raccourcis `RStudio` pour compiler votre projet {{%extlink "https://r-pkgs.org/workflows101.html"%}}.

{{%/tab%}}

{{%tab "RStudio"%}}
![](armadillo_rstudio.gif)
{{%/tab%}}

{{%/tabs%}}

{{%notice tip%}}
Pour vous familiariser avec l'interface et les raccourcis de `RStudio`, consultez la documentation {{%extlink "https://github.com/rstudio/cheatsheets/raw/master/rstudio-ide.pdf"%}}. Pour apprendre davantage sur le développement de paquetages R, consultez *R Packages* {{%extlink "https://r-pkgs.org/index.html"%}}.
{{%/notice%}}


## Sans RStudio

> Un projet C++, qui utilise Armadillo.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Installez la librairie sur votre système d'exploitation en suivant la documentation officielle {{%extlink "http://arma.sourceforge.net/download.html"%}}.
2. Testez la configuration de votre système avec l'exemple disponible sur la documentation officielle {{%extlink "http://arma.sourceforge.net/docs.html#example_prog"%}}.

{{%/tab%}}

{{%tab "Terminal"%}}
![](armadillo_cpp.gif)
{{%/tab%}}

{{%/tabs%}}
