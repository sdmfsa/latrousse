---
title: Charts
keywords: graphiques, r, charts, ggplot2, plot, histogram
description: Creating basic charts with R and ggplot2.
weight: 0
---

## Plot and ggplot2

You can create charts using the default R function `plot`. You can also use several packages to create all sorts of charts, e.g. the `qplot` function from `ggplot2` <a href="https://ggplot2.tidyverse.org/" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>:

{{%tabs%}}

{{%tab "Code"%}}

### Base

```r
ma_matrice <- matrix(c(2,3,5,7,2,4,3,2), 4, 2)
plot(ma_matrice^2)
# plot(ma_matrice^2, main="Mon Titre", xlab="Axe des X", ylab="Axe des Y")  # Remove the # to run
```

### ggplot2

```r
library(ggplot2)
ma_matrice_2 <- matrix(c(2,3,5,7,2,4,3,2), 4, 2)^2
mes_x <- ma_matrice_2[, 1]  # Column 1
mes_y <- ma_matrice_2[, 2]  # Column 2

ggplot2::qplot(mes_x, mes_y, main = "Mon titre", xlab="Axe des X", ylab="Axe des Y")
```

{{%/tab%}}

{{%tab "base"%}}
![](plot_output.png)
{{%/tab%}}

{{%tab "ggplot2"%}}
![](qplot_output.png)
{{%/tab%}}

{{%/tabs%}}


## Histogramme

We will use the historical prices obtained [here](../csv/#).

{{%tabs%}}

{{%tab "Code"%}}

### Base

1. Create a histogram of the prices in column `Open` from `donnees` using the `hist` command:

```r
hist(donnees$Open, main="Mon Titre", xlab="Open Price")
```


### ggplot2


1. Create a histogram of the prices in column `Open` from `donnees` using the `qplot` command:

```r
library(ggplot2)
ggplot2::qplot(donnees$Open, geom="histogram",
               main = "Mon titre", xlab="Open Price", ylab="Frequency")
```

{{%notice tip%}}
Save a copy of your graphic as a `png` or `pdf` file using the code below.
{{%/notice%}}

```r
png("histogramme.png")  # Début du fichier

# Le graphique
ggplot2::qplot(donnees$Open, geom="histogram",
               main = "Mon titre",
               xlab="Axe des X",
               ylab="Axe des Y")

dev.off()  # Fin, important !
```

> Use `pdf("histogramme.pdf")` instead of `png("histogramme.png")` to create a `pdf` file.


{{%/tab%}}

{{%tab "base"%}}
![](hist_base.png)
{{%/tab%}}

{{%tab "ggplot2"%}}
![](hist_ggplot2.gif)
{{%/tab%}}

{{%/tabs%}}









