---
title: "Langage R"
date: 2020
icon: "fab fa-r-project"
description: "Quelques exemples de code en R : installation, création de graphiques, etc."
keywords: r, installation de r , installer r, rstudio, plot, ggplot2
type : "docs"
weight: 1
---

