---
title: Statistics
keywords: descriptive statistics, mean, variance, quantiles, summary, stats
description: "R : How to compute the mean, variance, etc."
weight: 0
---

We will use data obtained [here](../csv/#plot-et-ggplot2).

> Read the data into R using the `read.csv` function and name it `mydata`.

## Summary

{{%tabs%}}

{{%tab "Code"%}}

1. Get a quick summary of the `mydata` table using `summary()`.
1. You can also save the results as a `csv` file with `write.csv`.

```r
mydata <- read.csv("monfichier.csv")
summary(mydata)
write.csv(summary(mydata), file = "sommaire.csv")
```

{{%/tab%}}

{{%tab "RStudio"%}}
![](summary.gif)
{{%/tab%}}


{{%/tabs%}}

## Mean and variance

{{%tabs%}}

{{%tab "Code"%}}

1. Get the mean of the `Open` column with `mean(mydata$Open)`.
1. Get the sample variance of the `Open` column with `var(mydata$Open)`.
1. Get the mean of each numeric column of `mydata` with `colMeans(mydata)`.
1. Get the variance of each numeric column of `mydata` using the `apply` function.

```r
mean(mydata$Open)
var(mydata$Open)
colMeans(mydata[, -1])
apply(mydata[, -1], 2, var)
```

> Since column `1` is not a numeric variable but a string, we need  to exclude it using the `[, -1]` syntax. More details [here](../notation/#indexation)

{{%/tab%}}

{{%tab "RStudio"%}}
![](mean-var.gif)
{{%/tab%}}

{{%/tabs%}}


## Covariance

{{%tabs%}}

{{%tab "Code"%}}

1. Get the covariance between columns `Open, High, Low, Close`  of `mydata`.

```r
var(mydata[, c("Open", "High", "Low", "Close")])
```

2. Save the resulting variance-covariance matrix on a `csv` file.


```r
write.csv(var(mydata[, c("Open", "High", "Low", "Close")]), file = "covariance.csv")
```

{{%/tab%}}

{{%tab "RStudio"%}}
![](var-covar.gif)
{{%/tab%}}

{{%/tabs%}}
