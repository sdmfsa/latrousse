---
title: Statistiques
keywords: statistiques descriptives, moyenne, variance, quantiles, summary, stats
description: "R : comment calculer la moyenne, la variance, etc."
weight: 0
---

Pour ces exemples, nous allons utiliser les données obtenues [ici](../csv/#plot-et-ggplot2).

> Lisez les données sur R avec la commande `read.csv` et nommez le tableau `mydata`.

## Sommaire

{{%tabs%}}

{{%tab "Code"%}}

1. Pour obtenir un sommaire rapide du tableau `mydata`, utilisez la fonctionn `summary()`.
1. Vous pouvez savegarder une copie des résultats en format `csv`  avec la fonction `write.csv`.

```r
mydata <- read.csv("monfichier.csv")
summary(mydata)  # Affiche résultat dans l'écran
write.csv(summary(mydata), file = "sommaire.csv")  # Sauvegarde en csv
```

{{%/tab%}}

{{%tab "RStudio"%}}
![](summary.gif)
{{%/tab%}}


{{%/tabs%}}

## Moyenne et variance

{{%tabs%}}

{{%tab "Code"%}}

1. Obtenez la moyenne de la colonne `Open` avec la commande `mean(mydata$Open)`.
1. Obtenez la variance échantillonnale de la colonne `Open` avec la commande `var(mydata$Open)`.
1. Obtenez la moyenne de chaque colonne numérique de `mydata` avec la commande `colMeans(mydata)`.
1. Obtenez la variance de chaque colonne numérique de `mydata` à l'aide de la fonction `apply`.

```r
mean(mydata$Open)
var(mydata$Open)
colMeans(mydata[, -1])
apply(mydata[, -1], 2, var)
```

> Puisque la colonne `1` n'est pas numérique mais plutôt une chaîne de caractères, il faut l'exclure avec la syntaxe `[, -1]`. Plus de détails [ici](../notation/#indexation)

{{%/tab%}}

{{%tab "RStudio"%}}
![](mean-var.gif)
{{%/tab%}}

{{%/tabs%}}


## Covariance

{{%tabs%}}

{{%tab "Code"%}}

1. Obtenez la covariance entre les colonnes `Open, High, Low, Close` du tableau `mydata`.

```r
var(mydata[, c("Open", "High", "Low", "Close")])
```

2. Sauvegardez la matrice variance-covariance résultante dans un fichier `csv`.


```r
write.csv(var(mydata[, c("Open", "High", "Low", "Close")]), file = "covariance.csv")
```

{{%/tab%}}

{{%tab "RStudio"%}}
![](var-covar.gif)
{{%/tab%}}

{{%/tabs%}}
