---
title: Regression
keywords: linear regression, lm, linear model
description: "R : parameter estimation using linear regression."
weight: 0
---

## Data

We will start by getting the dataset `Fama/French 3 Factors` under the `U.S. Research Returns Data` section  of Kenneth R. French's website <a href="http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/data_library.html#Research" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.

+ Download the `.zip` file and read the `.csv` file on `R`.

```r
facteurs_ff <- read.csv("~/Downloads/F-F_Research_Data_Factors.CSV",
                        skip=3)
```

+ You could also use the following code to download the data via `R` :

```r
adresse_url <- "http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/F-F_Research_Data_Factors_CSV.zip"

temp <- tempfile()
download.file(adresse_url, temp)
facteurs_ff <- read.csv(unz(temp, "F-F_Research_Data_Factors.CSV"), skip=3)
unlink(temp)
```


+ Delete the last `1131` lines from table `facteurs_ff` and convert each column from `character` to `numeric`.

```r
facteurs_ff <- facteurs_ff[-(1131:nrow(facteurs_ff)), ]
facteurs_ff <- apply(facteurs_ff, 2, as.numeric)
```

> Column `X` is the date formated as `YYYYMM`.

We need to follow the same steps with the second table `ports_ff` below. The data is available in `CSV` format <a href="http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/25_Portfolios_5x5_CSV.zip" target="_blank"><i class="fas fa-download"></i></a>.

```r
adresse_url <- "http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/25_Portfolios_5x5_CSV.zip"

temp <- tempfile()
download.file(adresse_url, temp)
ports_ff <- read.csv(unz(temp, "25_Portfolios_5x5.CSV"), skip=15)
unlink(temp)

ports_ff <- ports_ff[-(1131:nrow(ports_ff)), ]
ports_ff <- apply(ports_ff, 2, as.numeric)
```

> We have created two data tables : `facteurs_ff` et `ports_ff`.

## Sample

{{%notice note%}}
Same data used in table 2 of *Common risk factors in the returns on stocks and bonds*, Fama and French (1993) <a href="https://rady.ucsd.edu/faculty/directory/valkanov/pub/classes/mfe/docs/fama_french_jfe_1993.pdf" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.
{{%/notice%}}

### X : independent variables

+ Extract the data between `196307`  and `199112` from the `CSV` file <a href="http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/F-F_Research_Data_Factors_CSV.zip" target="_blank"><i class="fas fa-download"></i></a>. The output should have `342` observations for columns `X`, `Mkt.RF`, `SMB`, `HML` and `RF`.

```r
pos <- which(facteurs_ff[, "X"] <= 199112 &  # Data from above
             facteurs_ff[, "X"] >= 196307)
ports_1993 <- facteurs_ff[pos, ]
dim(ports_1993)  # (lignes, colonnes)
```

### Y : independent variables

+ Extract the data between `196307` and `199112` from the `CSV` file <a href="http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/25_Portfolios_5x5_CSV.zip" target="_blank"><i class="fas fa-download"></i></a>. The output should have `342` observations, each column representing a portfolio.

```r
pos <- which(ports_ff[, "X"] <= 199112 &
             ports_ff[, "X"] >= 196307)
ports_1993 <- ports_ff[pos, ]

dim(ports_1993)
```

## Descriptive statistics

### The Xs

Table 2 from Fama and French (1993) shows the averages and standard deviations of each time series as well as the autocorrelations with lags 1, 2 and 12.

+ Compare the statistics obtained from the `CSV` file to those of table 2 of Fama et French's article.


```r
moyennes <- colMeans(ports_1993[, -1])
ecarts_sd <- sqrt(diag(var(ports_1993[, -1])))
autocorr <- t(apply(ports_1993[, -1], 2,
                    function(x) acf(x, plot=F)[c(1,2,12)]$acf))
colnames(autocorr) <- c(c("Lag 1","Lag 2", "Lag 12"))
mat_corr <- cor(ports_1993[, -1])

format(round(cbind(moyennes, ecarts_sd, autocorr, mat_corr),2), nsmall=2)
```

{{%notice tip%}}
Export the tables in `html` or `LaTeX` format with the `xtable` package <a href="https://cran.r-project.org/web/packages/xtable/vignettes/xtableGallery.pdf" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.
{{%/notice%}}

```r
mon_tableau <- xtable::xtable(format(round(cbind(moyennes, ecarts_sd, autocorr, mat_corr),2), nsmall=2))
print(mon_tableau, file="tableau_2.tex")
print(mon_tableau, file="tableau_2.html", type="html")
```

### The Ys

We will compute the same statistics, but this time using the 25 portfolios.

+ Compare the statistics found below to those found in table 2 of Fama and French (1993).

```r
moyennes <- colMeans(ports_1993)
ecarts_sd <- sqrt(diag(var(ports_1993)))
format(round(cbind(moyennes, ecarts_sd),2), nsmall=2)
```

## Linear regression

For each portfolio $i$, we will find the value of $\beta_i$ in the following regression <a href="https://en.wikipedia.org/wiki/Capital_asset_pricing_model" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a> :

$$
E(R_i) - R_f = \alpha_i + \beta_i \left(E(R_m) - R_f\right)
$$

Where $ y_{i} = R_{i}- R_{f}$ and $x = R_{m}- R_{f}$. This data is found on variables `ports_1993` and `facteurs_1993` respectively.

The `lm(y ~ x)` function does a regression of variable `y` on `x`, where `x` is the independent variable. This expression returns a `liste` of class `lm`.

+ Find the estimates of $\beta$ for the first portfolio.

```r
y_ports <- ports_1993[, -1]  # Ys
x_facteurs <- facteurs_1993[, -1]  # Xs

## Port. 1 vs Mkt.RF
lm(y_ports[, 1] ~ x_facteurs[, 1])
```

+ Find the estimates of $\beta_i$ for all portfolios.

```r
## All ports
x <- x_facteurs[, 1]  # X doesn't change

betas <- apply(y_ports, 2, function(y) summary(lm(y ~ x))$coefficients["x", ])  # a and b
r2 <- apply(y_ports, 2, function(y) summary(lm(y ~ x))$r.squared)     # R^2
(tableau_4 <- format(round(rbind(betas, r2),2), nsmall=2))
```

+ Compare the results to those in table 4 of Fama and French (1993).

```r
tableau_4 <- format(round(rbind(betas, r2),2), nsmall=2)  # b and R^2
```
