---
title: Régression
keywords: regression lineaire, lm, lineaire
description: "R : estimation des paramètres d'une régression linéaire."
weight: 0
---


## Données

Nous allons commencer par obtenir le tableau de données `Fama/French 3 Factors` qui se trouve dans la section `U.S. Research Returns Data` sur le site de Kenneth R. French <a href="http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/data_library.html#Research" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.

+ Téléchargez le fichier `.zip` et ensuite lisez le fichier `.csv` sur `R`.

```r
facteurs_ff <- read.csv("~/Downloads/F-F_Research_Data_Factors.CSV",
                        skip=3)
```

+ Utilisez les commandes suivantes si vous préférez faire le téléchargement via `R` :

```r
adresse_url <- "http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/F-F_Research_Data_Factors_CSV.zip"

temp <- tempfile()
download.file(adresse_url, temp)
facteurs_ff <- read.csv(unz(temp, "F-F_Research_Data_Factors.CSV"), skip=3)
unlink(temp)
```


+ Supprimez les dernières `1131` lignes du tableau `facteurs_ff` et assurez-vous que les données numériques soient bien de type `numeric` au lieu de `character`.

```r
facteurs_ff <- facteurs_ff[-(1131:nrow(facteurs_ff)), ]
facteurs_ff <- apply(facteurs_ff, 2, as.numeric)
```

> La colonne `X` correspond à la date en format `YYYYMM`.

Nous devons suivre les mêmes pas avec le tableau `ports_ff` ci-bas. Les données sont disponibles en format `CSV` <a href="http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/25_Portfolios_5x5_CSV.zip" target="_blank"><i class="fas fa-download"></i></a>.

```r
adresse_url <- "http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/25_Portfolios_5x5_CSV.zip"

temp <- tempfile()
download.file(adresse_url, temp)
ports_ff <- read.csv(unz(temp, "25_Portfolios_5x5.CSV"), skip=15)
unlink(temp)

ports_ff <- ports_ff[-(1131:nrow(ports_ff)), ]
ports_ff <- apply(ports_ff, 2, as.numeric)
```

> Nous venons de créer deux tableaux : `facteurs_ff` et `ports_ff`.

## Échantillon

{{%notice note%}}
Mêmes données que le tableau 2 du papier *Common risk factors in the returns on stocks and bonds* de Fama et French (1993) <a href="https://rady.ucsd.edu/faculty/directory/valkanov/pub/classes/mfe/docs/fama_french_jfe_1993.pdf" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.
{{%/notice%}}

### X : Variables indépendantes

+ Obtenez les données entre `196307` et `199112` à partir du fichier `CSV` <a href="http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/F-F_Research_Data_Factors_CSV.zip" target="_blank"><i class="fas fa-download"></i></a>. Le tableau résultant devrait avoir `342` observations pour les colonnes `X`, `Mkt.RF`, `SMB`, `HML` et `RF`.

```r
pos <- which(facteurs_ff[, "X"] <= 199112 &  # Données obtenus ci-haut
             facteurs_ff[, "X"] >= 196307)
ports_1993 <- facteurs_ff[pos, ]
dim(ports_1993)  # (lignes, colonnes)
```

### Y : Variables dépendantes

+ Obtenez maintenant les données entre `196307` et `199112` à partir du fichier `CSV` <a href="http://mba.tuck.dartmouth.edu/pages/faculty/ken.french/ftp/25_Portfolios_5x5_CSV.zip" target="_blank"><i class="fas fa-download"></i></a>. Le tableau résultant devrait avoir `342` observations, où chaque colonne représente un portefeuille.

```r
pos <- which(ports_ff[, "X"] <= 199112 &
             ports_ff[, "X"] >= 196307)
ports_1993 <- ports_ff[pos, ]

dim(ports_1993)
```

## Statistiques descriptives

### Les X

Le tableau 2 de l'article de Fama et French (1993) affiche les moyennes et écarts-types de chaque série de données tout comme les autocorrélations décalées de 1, 2 et 12 unités de temps.

+ Comparez les statistiques descriptives obtenus à partir du ficher `CSV` à celles qui se trouvent sur le tableau 2 de l'article de Fama et French.


```r
moyennes <- colMeans(ports_1993[, -1])  # Ignorer la colonne date
ecarts_sd <- sqrt(diag(var(ports_1993[, -1])))
autocorr <- t(apply(ports_1993[, -1], 2,
                    function(x) acf(x, plot=F)[c(1,2,12)]$acf))
colnames(autocorr) <- c(c("Lag 1","Lag 2", "Lag 12"))
mat_corr <- cor(ports_1993[, -1])

format(round(cbind(moyennes, ecarts_sd, autocorr, mat_corr),2), nsmall=2)
```

{{%notice tip%}}
Exportez les tableaux en format `html` ou `LaTeX` avec le paquetage `xtable` <a href="https://cran.r-project.org/web/packages/xtable/vignettes/xtableGallery.pdf" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>.
{{%/notice%}}

```r
mon_tableau <- xtable::xtable(format(round(cbind(moyennes, ecarts_sd, autocorr, mat_corr),2), nsmall=2))
print(mon_tableau, file="tableau_2.tex")
print(mon_tableau, file="tableau_2.html", type="html")
```

### Les Y

Répétons les mêmes calculs, mais cette fois-ci avec les 25 portefeuilles formés par taille et valeur au livre/marché.

+ Comparez les statistiques obtenues à celles du tableau 2 de l'article de Fama et French.

```r
moyennes <- colMeans(ports_1993)
ecarts_sd <- sqrt(diag(var(ports_1993)))
format(round(cbind(moyennes, ecarts_sd),2), nsmall=2)
```

## Régression linéaire

Pour chaque portefeuille $i$, nous allons trouver la valeur de $\beta_i$ dans la régression suivante <a href="https://en.wikipedia.org/wiki/Capital_asset_pricing_model" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a> :

$$
E(R_i) - R_f = \alpha_i + \beta_i \left(E(R_m) - R_f\right)
$$

Où $ y_{i} = R_{i}- R_{f}$ et $x = R_{m}- R_{f}$. Ces données se trouvent déjà dans les variables `ports_1993` et `facteurs_1993` respectivement.

La commande `lm(y ~ x)` permet d'effectuer une régression de la variable `y` sur la variable `x`, où `x` est la variable indépendante. Cette expression retournera une `liste` de classe `lm`.

+ Trouvez les valeurs estimées de $\beta$ pour le premier portefeuille.


```r
y_ports <- ports_1993[, -1]  # Les Y
x_facteurs <- facteurs_1993[, -1]  # Les X

## Portefeuille 1 vs Mkt.RF
lm(y_ports[, 1] ~ x_facteurs[, 1])
```

+ Trouvez les valeurs estimées de $\beta_i$ pour tous les portefeuilles .

```r
## Tous les portefeuilles
x <- x_facteurs[, 1]  # X ne change pas

betas <- apply(y_ports, 2, function(y) summary(lm(y ~ x))$coefficients["x", ])  # a et b
r2 <- apply(y_ports, 2, function(y) summary(lm(y ~ x))$r.squared)     # R^2
(tableau_4 <- format(round(rbind(betas, r2),2), nsmall=2))
```

+ Comparez les résultats avec le tableau 4 de l'article de Fama et French (1993).

```r
tableau_4 <- format(round(rbind(betas, r2),2), nsmall=2)  # b et R^2
```
