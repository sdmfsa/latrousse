---
title: Installation
keywords: how, to, install, r, rstudio
description: How to install R and RStudio.
weight: 0
---

Follow these instructions to install `R` as well the `RStudio` integrated development environment.

## Download {#download}

{{%notice note%}}
R and Rstudio are available for `MAC`, `Windows` and `Linux`. It's possible to use **R** without **RStudio**, but not the opposite.
{{%/notice%}}

{{%tabs%}}
{{%tab "Instructions"%}}


### R

1. Go to `https://cran.r-project.org/`.
2. Click on `Download R` for your operating system.
3. Download the recommended installation file.
4. Open the downloaded file and follow the installation instructions.

### RStudio

1. Go to `https://rstudio.com/products/rstudio/download/`.
2. Download the installation file for your operating system by clicking on `DOWNLOAD`.
3. Open the downloaded file and follow the installation instructions.

{{%/tab%}}

{{%tab "R"%}}
![](install_r.gif)
{{%/tab%}}


{{%tab "RStudio"%}}
![](install_rstudio.gif)
{{%/tab%}}

{{%/tabs%}}

{{%notice tip%}}
For more details about the `RStudio` interface, check out the documentation <a href="https://github.com/rstudio/cheatsheets/raw/master/rstudio-ide.pdf" target="_blank"><i class="fas fa-file-pdf"></i></a>.
{{%/notice%}}


## R or RStudio

We will now test the `R` and `RStudio` installation.


{{%tabs%}}
{{%tab "Instructions"%}}

### R

1. Launch R.
2. Enter `print("Allô")` in the command line.

### RStudio

3. Launch RStudio.
2. Enter `print("Allô")` in the command line.

{{%/tab%}}

{{%tab "R"%}}
![](allo_r.gif)
{{%/tab%}}


{{%tab "RStudio"%}}
![](allo_rstudio.gif)
{{%/tab%}}

{{%/tabs%}}


## Packages

`R` offers more than **16 000** packages via CRAN <a href="https://cran.r-project.org/web/packages/" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>. Additional packages are available through Github/Gitlab/ Bitbucket/etc.


{{%tabs%}}

{{%tab "Instructions"%}}

1. Click on `Packages`.
1. Click on `Installer`.
1. Find a package, e.g. `ggplot2`.
1. Click on `Install`.
1. Import the newly installed package with `library(ggplot2)`.

{{%notice tip%}}
You can run the `install.packages("ggplot2")` command to install any package and you can also install multiple packages as in `install.packages(c("ggplot2", "shiny"))`.
{{%/notice%}}

{{%/tab%}}

{{%tab "RStudio"%}}
![](install_package.gif)
{{%/tab%}}

{{%/tabs%}}


### List of packages

Here are some resources to help you finding packages :

+ Web visualization with `htmlwidgets` ([ici](http://gallery.htmlwidgets.org/)).
+ Machine Learning ([ici](https://cran.r-project.org/web/views/MachineLearning.html)).
+ Finance ([ici](https://cran.r-project.org/web/views/Finance.html)).
+ Econometrics ([ici](https://cran.r-project.org/web/views/Econometrics.html)).
+ Optimization ([ici](https://cran.r-project.org/web/views/Optimization.html)).
+ Full list by subject ([ici](https://cran.r-project.org/web/views/)).
+ List of available packages on CRAN ([ici](https://cran.r-project.org/web/packages/)).
+ Liste ***Awesome R*** ([ici](https://github.com/qinwf/awesome-R))
+ Liste **RStudio** ([ici](https://support.rstudio.com/hc/en-us/articles/201057987-Quick-list-of-useful-R-packages))



