---
title: ANOVA
keywords: Analysis of variance, anova, aov
description: "R : analysis of variance (ANOVA) with R."
weight: 0
---

Here are the `R` commands you need to perform an analysis of variance using one or multiple factors.

## 1 Factor

Le following table shows some salaries [^1] divided into 3 different columns, i.e one column per activity sector.

[^1]: Data taken from *Statistics for Business and Financial Economics*, Lee, Lee et Lee (2013), chap. 12, pg. 574.

```r
salaires <- rbind(c(755,520,438),
                  c(712,295,828),
                  c(845,553,622),
                  c(985,950,453),
                  c(1300,930,562),
                  c(1143,428,348),
                  c(733,510,405),
                  c(1189,864,938))
salaires <- as.data.frame(salaires)
colnames(salaires) <- c("Banks", "Utilities", "Computers")
```
```
  Banks Utilities Computers
1   755       520       438
2   712       295       828
3   845       553       622
4   985       950       453
5  1300       930       562
6  1143       428       348
```

We are interested in knowing whether the mean of all salaries differs statistically from the mean of each group. This is our null hypothesis :

$$
H_0 : \mu_1 = \mu_2  = \mu_3
$$

The `aov` function helps us answer this question using an analysis of variance. We must first prepare our data using the `stack` function to follow the required data structure as shown below.

```r
salaires <- stack(salaires)
```

```
  values   ind
1    755 Banks
2    712 Banks
3    845 Banks
4    985 Banks
5   1300 Banks
6   1143 Banks
```

> All salaries are now in the `values` column, the sector names are in the `ind` column.

We might want to visualize the data with a `boxplot` :

```r
boxplot(values ~ ind, data = salaires, col= rainbow(3),
        main = "Salaires pour 3 groupes de hauts dirigeants")
```

![](graphique.png)

And here are the results of a **one factor** analysis of variance :

```r
aov_out <- aov(values ~ ind, data = salaires)
summary(aov_out)
```
```
            Df  Sum Sq Mean Sq F value  Pr(>F)
ind          2  685129  342565   6.451 0.00655 **
Residuals   21 1115232   53106
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
```

## 2 Factors

From  following the same steps as in the one factor case, we know that we must first prepare our dataset, we know need two extra columns instead of one to group the salaries [^2]. We now have two groups : years of experience from 1 to 3 as well as 4 different regions.

[^2]: idem, page 564.

```r
salaires <- as.data.frame(
  rbind(c(16,16.5,19,17,24,25),
        c(21,20.5,20,19,21,22.5),
        c(18,19,21,20.9,22,21),
        c(13,13.5,20,20.8,25,23))
)
colnames(salaires) <- c(1, 1, 2, 2, 3, 3)

salaires <- stack(salaires)
salaires$ind <- substr(salaires$ind, 1, 1)
salaires$region <- as.character(rep(1:4, 6))
```

Which gets us a `data.frame` with three columns, where two of them correspond to the two factors `ind` (years of experience) and `region`.

```
   values ind region
1    16.0   1      1
2    21.0   1      2
3    18.0   1      3
4    13.0   1      4
5    16.5   1      1
```

And here are the results of a **two factor** analysis of variance :

### Without interaction

```r
aov_out <- aov(values ~ region + ind, data = salaires)
summary(aov_out)
```
```
            Df Sum Sq Mean Sq F value   Pr(>F)
region       3   7.92    2.64   0.556 0.651072
ind          2 132.90   66.45  13.981 0.000217 ***
Residuals   18  85.55    4.75
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
```

### With interaction

```r
aov_out <- aov(values ~ region * ind, data = salaires)
summary(aov_out)
```
```
            Df Sum Sq Mean Sq F value   Pr(>F)
region       3   7.92    2.64   4.049   0.0334 *
ind          2 132.90   66.45 101.907 2.96e-08 ***
region:ind   6  77.73   12.95  19.867 1.39e-05 ***
Residuals   12   7.82    0.65
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
```

## Application

The following example uses data obtained through the `EQS` function on `Bloomberg`. The following  filters were used :

![](eqs.png)

> Export the results to `Excel` and then save a copy as a `CSV` file. Here we kept the data only and saved it as a `CSV` file.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Read the data into `R` :

```r
eqs_toronto <- read.csv("~/Downloads/eqs_toronto.csv")

eqs_toronto <- eqs_toronto[!(eqs_toronto$Ind.Subgroup == "N/A"), ]
eqs_toronto <- eqs_toronto[!(eqs_toronto$YTD.Tot.Ret == "#N/A N/A"), ]
eqs_toronto <- eqs_toronto[!(eqs_toronto$GICS.Sector == "N/A"), ]

eqs_toronto$YTD.Tot.Ret <- as.numeric(eqs_toronto$YTD.Tot.Ret)
eqs_toronto$X1M.Tot.Ret <- as.numeric(eqs_toronto$X1M.Tot.Ret)
nb_sectors <- length(unique(eqs_toronto$GICS.Sector))
```

2. Obtain the analysis of variance results using the following column values :

+ `YTD.Tot.Ret`
+ `RSI..Period.30`
+ `X1M.Tot.Ret`

With one and two factors :

+ `GICS.Sector`
+ `Cntry.of.Risk`

{{%/tab%}}


{{%tab "R"%}}

![](aov.gif)

{{%/tab%}}

{{%tab "?"%}}

```r
## GICS
boxplot(YTD.Tot.Ret ~ GICS.Sector, data = eqs_toronto, col= rainbow(nb_sectors))
aov_out <- aov(YTD.Tot.Ret ~ GICS.Sector, data = eqs_toronto)
summary(aov_out)

## RSI
boxplot(RSI..Period.30 ~ GICS.Sector, data = eqs_toronto, col= rainbow(nb_sectors))
aov_out <- aov(RSI..Period.30 ~ GICS.Sector, data = eqs_toronto)
summary(aov_out)

## 1 Month
boxplot(X1M.Tot.Ret ~ GICS.Sector, data = eqs_toronto, col= rainbow(nb_sectors))
aov_out <- aov(X1M.Tot.Ret ~ GICS.Sector, data = eqs_toronto)
summary(aov_out)

## Two way without interactions
aov_out <- aov(RSI..Period.30 ~ GICS.Sector + Cntry.of.Risk, data = eqs_toronto)
summary(aov_out)

## Two way with interactions
aov_out <- aov(RSI..Period.30 ~ GICS.Sector * Cntry.of.Risk, data = eqs_toronto)
summary(aov_out)
```

{{%/tab%}}


{{%/tabs%}}


