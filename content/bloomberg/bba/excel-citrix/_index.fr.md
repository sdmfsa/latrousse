---
title: Fichiers Excel
keywords: sauvegarder un tableau, exporter un tableau, télécharger un tableau, excel bloomberg, bloomberg anywhere
description: Comment télécharger ou téléverser Excel avec la version web de Bloomberg.
weight: 0
---

Si vous utilisez un **terminal Bloomberg**[^1], tout fichier téléchargé à partir de Bloomberg sera sauvegardé automatiquement dans le dossier `C:\blp\data` (ou similaire) sur **votre ordinateur**. Vous pouvez sauvegarder une copie dans un autre dossier en cliquant sur `Fichier`  et ensuite sur `Enregistrer sous` , comme avec tout fichier Excel.

[^1]: Les terminaux Bloomberg sont uniquement compatibles avec Windows.

{{%notice note%}}
Si vous travaillez avec la version web de Bloomberg, il faut suivre les instructions ci-bas pour enregistrer une copie d'un fichier sur **votre ordinateur**.
{{%/notice%}}

## Version web

{{%tabs%}}
{{%tab "Instructions"%}}

1. À partir du fichier Excel que vous venez d'exporter, cliquez sur `Fichier` et ensuite sur `Enregistrer sous`[^1] pour enregistrer une copie des données.

[^1]: Vous pouvez aussi appuyer sur la touche `<F12>` pour ouvrir la fenêtre `Enregistrer sous`.

2. Cliquez sur le menu Citrix en haut de l'écran et ensuite sur le bouton `Download`.

3. Choisissez le fichier à télécharger et cliquez sur `Open`. Vous trouverez le fichier téléchargé dans votre dossier `Téléchargements`.

{{%/tab%}}
{{%tab "Terminal"%}}

![](hp_export.gif)

{{%/tab%}}
{{%/tabs%}}
