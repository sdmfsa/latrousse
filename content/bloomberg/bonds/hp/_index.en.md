---
title: Historical data
keywords: historical data, bonds price, hp table, export prices, bonds data
description: "[Bonds] Downloading historical data with Bloomberg"
weight: 0
---

It is possible to access some of Bloomberg's data via Excel thanks to the Bloomberg API. All you need to know are the name of the security and the data field (**mnemonic**) of interest. For instance, `PX_LAST` is the data field used to get the last price provided by the exchange while the `YLD_YTM_MID` field provides the yield of a bond based on its *Mid* price.

{{%notice tip%}}
Use the `FLDS` function on the terminal to find a data field **mnemonic** as well as all details available on a given data field.
{{%/notice%}}

## API via Excel

{{%tabs%}}
{{%tab "Instructions"%}}

1. Click on the `Bloomberg` tab and then on `Spreadsheet Builder`.

2. Under *Analyze Historical Data*, click on `Historical Data Table` and then on `Next`.

3. Choose the financial securities of interest.

4. Choose the data fields for your query.

5. Choose the start and end dates as well as the data frequency for your query.

6. Choose the output cell on Excel and click on `Finish`.

{{%/tab%}}
{{%tab "Terminal"%}}

![](bonds_hp.gif)

{{%/tabs%}}
{{%/tabs%}}

