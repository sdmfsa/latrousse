---
title: Données historiques
keywords: données historiques, prix obligations, tableau hp, exporter prix, données obligations
description: "[Obligations] Comment obtenir des données historiques avec Bloomberg"
weight: 0
---

Il est possible d'obtenir des données via Excel grâce à l'API Bloomberg. Pour ce faire, il suffit de choisir le **titre** et le nom du champ d'extraction (**mnémoniques**) souhaité. Par exemple `PX_LAST` est le nom du champ correspondant à la cote de fermeture tandis que `YLD_YTM_MID` fait référence au rendement à l'échéance basé sur le prix *Mid*.

{{%notice tip%}}
Utilisez la fonction `FLDS` sur le terminal pour trouver le **mnémonique** des champs d'extraction ainsi que tout autre détail disponible sur un champ de données.
{{%/notice%}}

## API via Excel

{{%tabs%}}
{{%tab "Instructions"%}}

1. Cliquez sur l'onglet `Bloomberg` et ensuite sur `Spreadsheet Builder`.

2. Dans la sous section *Analyze Historical Data*, cliquez sur `Historical Data Table` en ensuite sur `Next`.

3. Sélectionnez le ou les titres dont vous désirez extraire les données.

4. Sélectionnez les champs d'extraction souhaités.

5. Choisissez les dates de début et de fin de l'historique ainsi que la fréquence de données.

6. Sélectionnez la cellule Excel où vos données seront placées et cliquez sur `Finish`.

{{%/tab%}}
{{%tab "Terminal"%}}

![](bonds_hp.gif)

{{%/tabs%}}
{{%/tabs%}}

