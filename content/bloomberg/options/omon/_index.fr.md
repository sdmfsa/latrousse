---
title: Monitoring
keywords: produits, dérivés, monitoring, monitorage, de, options, achat, vente, omon
description: Suivi de données sur des options en temps réel avec Bloomberg.
weight: 0
---

La fonction `OMON` permet de faire un suivi en temps réel des données portant sur les options grâce à une interface personnalisable.

{{%tabs%}}
{{%tab "Instructions"%}}

1. Rentrez le ticker Bloomberg d'un actif sous-jacent sur la ligne de commande Bloomberg, par exemple `POW CN Equity`, et appuyez sur `<Entrée>`.

2. Tapez `OMON` et appuyez sur `<Entrée>` pour lancer la fonction `Option Monitor`.

3. Choisissez les paramètres d'affichage sur le panneau supérieur, nommé la `Zone de Contrôle`.

{{%/tab%}}
{{%tab "Help"%}}

+ Centrez la chaîne d'options à l'aide du champ `Center`.
+ Contrôlez le nombre de prix d'exercice affichés avec le paramètres `Strikes`.
+ Choisissez la date d'échéance du contrat avec le champ `Exp.`.
+ Accédez à des données historiques (max. 120 jours) en modifiant la date du champ `As Of`.

{{%notice tip%}}
Il est possible de connaître tous les détails sur la fonction `OMON` en accédant à la documentation officielle. Tapez `HELP OMON` et appuyez sur la touche `<Entrée>` pour en savoir plus !
{{%/notice%}}

{{%/tab%}}


{{%tab "Terminal"%}}

![](omon_use.gif)

{{%/tab%}}

{{%/tabs%}}
