---
title: Prices
keywords: historical data, historical prices, hp table, export prices, bloomberg data, get prices, bloomberg api, api
description: Obtaining historical prices with Bloomberg.
weight: 0
---

Bloomberg's `HP` function offers access to historical data on the terminal. You can also export this data to Excel if needed by following the steps below.

## `HP` function

{{%tabs%}}
{{%tab "Instructions"%}}

1. Load a security on the terminal using its ticker, e.g. `TSLA US Equity`, and hit the `Enter` key.

{{%notice tip%}}
Instead of typing the word `EQUITY`, hit the `<F8>` key to get the word `Equity`.
{{%/notice%}}

2. Type `HP` on the Bloomberg command line and hit `Enter` to access the `Historical Price Table` function.

3. Choose the desired parameters for the data table.

{{%/tab%}}
{{%tab "Terminal"%}}

![](hp_use.gif)

{{%/tabs%}}
{{%/tabs%}}

## Exporting the `HP` table

{{%tabs%}}

{{%tab "Instructions"%}}

### Bloomberg terminal
1. From within the `HP` function window, click on the `96) Export` button. If you prefer, you can type the number next to `96) Export` in the command line instead and hit `Enter`, i.e. `96 <Enter>`.
2. Go to the Excel file you just exported and click on `File`, then on `Save as`[^1] to save a copy of the current file.
[^1]: You can also press the `<F12>` key to open the `Save as` window.

{{%notice note%}}
Since you are using a Bloomberg terminal, this will open an Excel file with the data on **your computer**. If you are accessing Bloomberg via **Citrix**, including the web-based version, you must follow [this instructions](../../bba/excel-citrix/#web-version) to save a local copy of the file.
{{%/notice%}}

{{%/tab%}}

{{%tab "Terminal"%}}

### Physical Bloomberg terminal
![](hp_export_win.gif)
### Bloomberg Anywhere
![](hp_export.gif)

{{%/tab%}}

{{%/tabs%}}
