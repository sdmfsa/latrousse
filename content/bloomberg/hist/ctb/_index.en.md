---
title : Financial Analysis
keywords: financial statements, income statement, balance sheet, financial ratios, financial analysis with Bloomberg,fa
description: Finding financial statements data with Bloomberg
weight: 0
---


## `FA` Tables

{{%tabs%}}

{{%tab "Instructions"%}}

### `FA` function

1. Load a security on a terminal window by entering its Bloomberg ticker followed by the `<Enter>` key, e.g. `MSFT US Equity <Enter>`.

2. Type `FA` on the Bloomberg command line and then hit `<Enter>`.

### Exporting the `FA` table

1. From within the `FA` function, click on `Export` and then on `Current Template` to export a copy of the data table to Excel.


{{%/tab%}}
{{%tab "Terminal"%}}

### `FA` function

1. `MSFT US <F8> <Enter>`

![](fa_1.png)

2. `FA <Enter>`

![](fa_2.png)

### Exporting the `FA` table

1. `97 <Enter>` and then `73 <Enter>`

![](fa_3.png)

### `XLTP` templates

1. `XLTP XFA <Enter>` followed by `1 <Enter>`

![](xltp_1.png)

![](xltp_3.png)

{{%/tab%}}

{{%/tabs%}}

{{%notice tip%}}
Type `XLTP XFA` on the Bloomberg command line and then hit `<Enter>` to access the `Financial Analysis - XLTP XFA` Excel template and then click on `Open`.
{{%/notice %}}
