---
title : Analyse financière
keywords: états financiers, état des résultats, bilan, ratios financiers, analyse financière avec Bloomberg, fa
description: Données sur les  états financiers avec Bloomberg.
weight: 0
---


## Tableaux `FA`


{{%tabs%}}

{{%tab "Instructions"%}}

### Fonction `FA`

1. Rentrez un ticker sur la ligne de commande Bloomberg et appuyez sur la touche `<Entrée>`, par exemple `MSFT US Equity <Entrée>`.

2. Tapez `FA` sur la ligne de commande et appuyez sur la touche `<Entrée>`.

### Exporter le tableau `FA`

1. À partir de la fonction `FA`, cliquez sur `Export` et ensuite sur `Current Template` pour télécharger une copie du tableau de données sur Excel.



{{%/tab%}}
{{%tab "Terminal"%}}

### Fonction `FA`

1. `MSFT US <F8> <Entrée>`

![](fa_1.png)

2. `FA <Entrée>`

![](fa_2.png)

### Exporter le tableau `FA`

1. `97 <Entrée>` et après `73 <Entrée>`

![](fa_3.png)

### Gabarits `XLTP`

1. `XLTP XFA <Entrée>` et ensuite `1 <Entrée>`

![](xltp_1.png)

![](xltp_3.png)

{{%/tab%}}

{{%/tabs%}}


{{%notice tip%}}
Tapez `XLTP XFA` sur la ligne de commande Bloomberg et appuyez sur la touche `<Entrée>` pour accéder au gabarit Excel `Financial Analysis - XLTP XFA`. Cliquez sur `Open` pour ouvrir le gabarit sur Excel.
{{%/notice %}}
