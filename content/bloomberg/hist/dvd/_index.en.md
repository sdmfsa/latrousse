---
title: Dividends
keywords: dividends data, dividends, dvd function, dvd, excel dividends, export dividends, export dvd
description: Finding and downloading dividend data with Bloomberg.
weight: 0
---

Bloomberg's `DVD` function offers access to dividends and split data on the terminal. You can also obtain this data from Excel if needed by following the steps below.

## `DVD` function

{{%tabs%}}
{{%tab "Instructions"%}}

1. Load a security on the terminal using its ticker, e.g. `RCI/B CN Equity`, and hit the `Enter` key.

{{%notice tip%}}
Instead of typing the word `EQUITY`, hit the `<F8>` key to get the word `Equity`.
{{%/notice%}}

2. Type `DVD` on the Bloomberg command line and hit `Enter` to access the `Dividend/Split Summary` function.

{{%/tab%}}
{{%tab "Terminal"%}}

![](dvd_use.gif)

{{%notice note%}}
It is currently not possible to export de `DVD` table to Excel from the terminal. Instead, you need to use an Excel formula to recreate the table as explained below.
{{%/notice%}}

{{%/tab%}}
{{%/tabs%}}

## Exporting the `DVD` table

{{%tabs%}}
{{%tab "Instructions"%}}

1. From Excel, enter the following Bloomberg `BDS` formula to retrieve the full `DVD` table for `RCI/B CN Equity`:

```
=BDS("RCI/B CN EQUITY","DVD_HIST_ALL", "Headers", "Yes")
```

You can set additional parameters such as specific start and end dates if needed as in the following example:

```
=BDS("RCI/B CN EQUITY","DVD_HIST_ALL", "DVD_START_DT=20100101", "DVD_END_DT=20200601")
```

{{%notice note%}}
If you are using Bloomberg Anywhere via **Citrix**, including the web-based version, you must follow [this](http://sdmapps.ca/latrousse/bloomberg/bba-download/) instructions to save a local copy of the Excel file.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](dvd_export.gif)

{{%/tab%}}
{{%/tabs%}}
