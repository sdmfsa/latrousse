---
title: Dividendes
keywords: dividendes, dividendes historiques, données dividendes, fonction dvd, dividendes excel, exporter dividendes, exporter dvd
description: Obtention et téléchargement de dividendes avec Bloomberg.
weight: 0
---

La fonction Bloomberg `DVD` permet d'obtenir des données historiques sur les dividendes et les fractionnement d'actions. Il est aussi possible d'obtenir ce tableau de données sur Excel en suivant les instructions ci-bas.

## Fonction `DVD`

{{%tabs%}}
{{%tab "Instructions"%}}

1. Rentrez le ticker Bloomberg du titre recherché, par exemple `RCI/B CN Equity`, et appuyez sur la touche `Entrée`.

{{%notice tip%}}
Au lieu de taper le mot `EQUITY`, appuyez sur la touche `<F8>` pour rentrer le mot `Equity`.
{{%/notice%}}

2. Tapez `DVD` sur la ligne de commande Bloomberg suivi de la touche `Entrée` pour accéder à la fonction `Dividend/Split Summmary`.

{{%/tab%}}
{{%tab "Terminal"%}}

![](dvd_use.gif)

{{%notice note%}}
Remarquez que ce n'est pas possible d'exporter le tableau de la fonction `DVD` à partir du terminal. Pour y parvenir, il faut le recréer avec une formule Excel.
{{%/notice%}}

{{%/tab%}}
{{%/tabs%}}

## Exporter le tableau `DVD`

{{%tabs%}}
{{%tab "Instructions"%}}

1. À partir d'Excel, rentrez la formule Bloomberg `BDS` suivante pour obtenir le tableau `DVD` complet du titre `RCI/B CN Equity`:

```
=BDS("RCI/B CN EQUITY","DVD_HIST_ALL", "Headers", "Yes")
```

Vous pouvez aussi utiliser des paramètres additionnels pour spécifier une date de début comme dans l'exemple ci-bas :

```
=BDS("RCI/B CN EQUITY","DVD_HIST_ALL", "DVD_START_DT=20100101", "DVD_END_DT=20200601")
```

{{%notice note%}}
Si vous utilisez Bloomberg Anywhere via **Citrix**, incluant la version web, vous devez suivre [ces](http://sdmapps.ca/latrousse/fr/bloomberg/bba-download/) instructions pour sauvegarder une copie du fichier Excel sur **votre ordinateur**.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](dvd_export.gif)


{{%/tab%}}
{{%/tabs%}}
