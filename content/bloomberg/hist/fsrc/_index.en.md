---
title: Fund screening
keywords: fund, screening, closed, open, end, mutual, price, excel, download
description: The FSRC function lets us apply several filters to find funds.
weight: 0
---

We will use the `FSRC` function to find a list of Canadian closed end funds. The resulting list of Bloomberg tickers (Unique identifiers) will then be used to obtain daily historical data.

## `FSRC` - Function

{{%tabs%}}
{{%tab "Instructions"%}}

1. Type `fund screening` on the command line and click on `FSRC`.

2. Click on `Fund type` and choose `Closed end`.

3. Click on `Country of domicile` and choose `Canada`.

4. Click on `Results`.

{{%notice tip%}}
To learn more about the filters available, hit the `<F1>` key from the `FSRC` function.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](bng-fsrc.gif)

{{%/tabs%}}
{{%/tabs%}}

## Data via `Excel`

{{%tabs%}}
{{%tab "Instructions"%}}

1. From the Results page of the `FSRC` function, click on `Export`.

2. From the `Excel` file you just exported, click on the `Bloomberg` tab and then on `Spreadsheet builder`.

3. Click on `Historical Data Table` and then on `Next`.

4. Choose the `Excel` range containing the tickers (unique identifiers) and click on `Next`.

5. Use some keywords to find the data fields, e.g. `Net Asset Value`. Choose the `FUND_NET_ASSET_VAL` field and click on `Add`, then on `Next`.

6. Adjust the query parameters, e.g. the data frequency and dates. Then click on `Next`.

7. Choose the `Excel` range where the resulting data will be placed and click on `Finish`.

{{%notice tip%}}
If you are using the **web version**, you must follow [these instructions](http://sdmapps.ca/latrousse/fr/bloomberg/bba/excel-citrix/) to download the data from Excel to your local computer.
{{%/notice%}}

{{%/tab%}}
{{%tab "Terminal"%}}

![](bng-fsrc-excel.gif)

{{%/tabs%}}
{{%/tabs%}}
