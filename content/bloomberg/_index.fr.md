---
title: "Bloomberg"
date: 2020
icon: "ti-pulse"
description: Le terminal Bloomberg vous aide à trouver et à analyser des données financières historiques et en temps-réel.
keywords: bloomberg, connexion, bmc, certificat, drs, bba, anywhere
type : "docs"
weight: 2
---

