---
title: Installation
keywords: office 365, installation, office online, excel installation
description: Installing the latest version of Excel.
weight: -1
---

# Office 365

## Web access and installation

{{%notice info%}}
These instructions assume that you have access to a Microsoft Office 365 account. Université Laval <a href= "https://www4.fsa.ulaval.ca/etudiants-actuels/services-technologiques/logiciels/" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a> students and staff may use their `IDUL` followed by `@ulaval.ca` to log in.
{{%/notice%}}

1. Go to the Office 365 <a href="https://portal.office.com" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a> login page.

2. Use your `idul@ulaval.ca` account to log in.

![`portal.office.com`](login.png)

3. Click on `Install Office`.

![`Home`](home.png)

![`Install`](install.png)

{{% notice note%}}

Follow the installation wizard instructions to complete the installation.
{{% /notice%}}

## Excel login

1. Launch `Excel` **locally**.
2. Click on `Account > Sign in`.

![`Home`](excel_home.png)

3. Log in using your **`IDUL@ulaval.ca`** e-mail.

![`Login Excel`](excel_login.png)

![`Login UL`](excel_ul_login.png)

4. Make sure you are using your **`@ulaval.ca`** account.

![`Connection OK`](excel_ok.png)

{{% notice tip%}}
Once you are connected you may access your OneDrive files directly from Excel (`CTRL + O`).
{{% /notice %}}

## Opening a OneDrive file

1. Launch `Excel`.
2. Click on `File > Open` ( shortcut : `CTRL + O`) and use the file browser to find your file.

![`Open`](ouvrir.png)

![`Opened`](ouvert_ok.png)


