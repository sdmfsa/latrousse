---
title: Notation
keywords: formule matricielle, noms
description: Un tour de quelques concepts Excel de base.
weight: -3
---

La notation `A1` fait référence à la colonne `A`, ligne `1` d'un chiffrier Excel. Ainsi, la notation `A1 = 2` indique que le nombre `$2$` se trouve dans la cellule `A1`.

## Vecteurs

Sur Excel, on représente un vecteur comme une ligne ou une colonne. Par exemple, nous pouvons représenter le vecteur `$(2,3,5,7)$` sur Excel en écrivant le nombre `$2$` dans la cellule `A1`, le nombre `$3$` dans la cellule `A2`, le nombre `$5$` dans la cellule `A3` et le nombre `$7$` dans la cellule `A4`. Par défaut, Excel nomme cette plage de données (notre vecteur) `A1:A4`, où `A1 = cellule de départ`, `A4 = cellule de fin`.

![`A1:A4 = (2,3,5,7)`](excel-c.png)

<i class="fa fa-table"></i> Pour calculer la longueur du vecteur `A1:A4` sur Excel, il suffit de faire appel à la fonction `NB()` <a href="https://support.office.com/fr-fr/article/nb-nb-fonction-a59cd7fc-b623-4d93-87a4-d23bf411294c?omkt=fr-CA" target="_blank"><i class="fa fa-external-link-alt"></i></a>. Par exemple, la commande Excel `=NB(A1:A4)` retourne `4`, soit le nombre d'éléments qui se trouvent dans le vecteur.

![`A5 = NB(A1:A4)`](excel-nb.png)

Parfois, c'est préférable de modifier les noms par défaut (`A1:A4`) et d'utiliser un nom personnalisé. Par exemple, on peut choisir d'appeler le vecteur `MonVecteur` au lieu d'utiliser le nom par défaut `A1:A4`. Pour ce faire, il suffit de sélectionner la plage où se trouve notre vecteur et de cliquer sur la boîte qui se trouve au coin supérieur gauche du tableau. Ensuite, on peut taper n'importe quel nom respectant les règles de syntaxe <a href="https://support.office.com/fr-fr/article/d%c3%a9finir-et-utiliser-des-noms-dans-les-formules-4d0f13ac-53b7-422e-afd2-abd7ff379c64?omkt=fr-CA&ui=fr-FR&rs=fr-CA&ad=CA" target="_blank"><i class="fa fa-external-link-alt"></i></a>.

![`MonVecteur = A1:A4`](excel-names.png)

<i class="fa fa-table"></i> Pour calculer la longueur du vecteur `MonVecteur` sur Excel, il suffit d'utiliser le nom `Monvecteur` au lieu de `A1:A4` au moment de faire appel à la fonction `NB()`.

![`A5 = NB(MonVecteur)`](excel-names-2.png)

## Matrices

Sur Excel, on représente une matrice `$M_{i\times j}$` comme un tableau ayant `$i$` lignes et `$j$` colonnes. Par exemple, nous pouvons représenter la matrice

$$
M_{4\times2}=
  \begin{bmatrix}
    2 & 2 \\\\
    3 & 4 \\\\
    5 & 3 \\\\
    7 & 2
  \end{bmatrix}
$$

comme un tableau ayant `$4$` lignes et `$2$` colonnes. Nous pouvons faire référence à cette matrice avec la syntaxe de base (`A1:B4`), mais nous pouvons aussi nommer cette matrice `MaMatrice` et y faire référence avec ce nom :

![`MaMatrice = A1:B4`](excel-matrix.png)

<i class="fa fa-table"></i> Nous pouvons par exemple élever chaque nombre de la matrice à une puissance quelconque :

![`{D1:E4=MaMatrice^2}`](excel-matrix-2.png)

<i class="fa fa-exclamation-triangle"></i> Les accolades `{}` indiquent qu'il s'agit d'une formule matricielle <a href="https://support.office.com/fr-fr/article/instructions-et-exemples-de-formules-matricielles-7d94a64e-3ff3-4686-9372-ecfd5caa57c7?omkt=fr-CA&ui=fr-FR&rs=fr-CA&ad=CA" target="_blank"><i class="fa fa-external-link-alt"></i></a>.

## Indexation

Sur Excel, on utilise la fonction `INDEX()` pour accéder aux éléments d'un vecteur ou d'une matrice. Par exemple, la commande Excel `INDEX(MonVecteur, 3)` retourne le 3e élément du vecteur `A1:A4` tandis que la commande `INDEX(MaMatrice, 3, 2)` retourne l'élément se trouvant dans la ligne `3`, colonne `2`, de la matrice `A1:B4`.

![`A6=INDEX(MonVecteur, 3)`](excel-index.png)

