---
title: CSV files
keywords: read csv, read data excel, import csv, comma separated, csv, data
description: How to read a CSV file on Excel.
weight: -1
---

{{%notice tip%}}
Consult the official Excel documentation for more details on importing data <a href="https://support.office.com/fr-fr/article/importer-ou-exporter-des-fichiers-texte-txt-ou-csv-5250ac4c-663c-47ce-937b-339e391393ba?omkt=fr-CA&ui=fr-FR&rs=fr-CA&ad=CA" target="_blank"><i class="fa fa-external-link-square-alt mb-0"></i></a>  and the  *Power Query* tool <a href="https://support.microsoft.com/fr-fr/office/importer-ou-exporter-des-fichiers-texte-txt-ou-csv-5250ac4c-663c-47ce-937b-339e391393ba" target="_blank"><i class="fa fa-external-link-square-alt mb-0"></i></a>.
{{%/notice%}}

## Text Import Wizard

{{%tabs%}}
{{%tab "Instructions"%}}

1. Open the `.csv` file with Excel.
2. Click on the column header, e.g. column `A`.
3. Click on `Data > Convert `.
4. Follow the instructions and click on `Finish`.
5. Save a copy of the file using the `.xlsx` format by clicking on `File > Save as`.

{{%/tab%}}
{{%tab "Excel"%}}

![](csv_excel.gif)

{{%/tab%}}
{{%/tabs%}}


## Power Query

{{%tabs%}}
{{%tab "Instructions"%}}

1. From an open `.xlsx` file, click on `Data > Get Data > From a file > From a text/CSV file`.
2. Choose the `.csv` file to import into `Excel`.
3. Make sure tha the delimiter is correct, e.g. `Comma`, and click on `Load`.

{{%/tab%}}
{{%tab "Excel"%}}

![](csv_excel_2.gif)

{{%/tab%}}
{{%/tabs%}}
