---
title: Charts
keywords: excel chart, finance chart, stock chart, manual chart, choose data, draw, manually
description: Excel's recommended charts and chart design options.
weight: -2
---

## Recommended charts

1. Insert a new [chart](https://support.office.com/fr-fr/article/types-de-graphique-disponibles-dans-office-a6187218-807e-4103-9e0a-27cdb19afb90?omkt=fr-CA&ui=fr-FR&rs=fr-CA&ad=CA#OfficeVersion=Windows) : `CTRL + A > Insert > Candlestick Stock chart`
![](1_ohlcv.png)

2. Make sure your data is positioned as expected :
![](2_ohlcv_error.png)

3. Add a title, label, etc.
![](3_ohlcv_chart.png)

## Chart design

1. Insert a new (empty) chart : `Insert > Scatter chart`
![](1_xlsx_insert_chart.png)

2. Click on the empty chart.
![](2_xlsx_double_click.png)

3. Add your data.
![](3_xlsx_select_data_port.png)

4. Select a predefined [chart style](https://support.office.com/fr-fr/article/modifier-la-disposition-ou-le-style-d-un-graphique-a346e438-d22a-4540-aa87-bce9feb719cf?omkt=fr-CA&ui=fr-FR&rs=fr-CA&ad=CA).

![](4_xlsx_layout.png)

5. Sample output.

![](5_xlsx_chart.png)

