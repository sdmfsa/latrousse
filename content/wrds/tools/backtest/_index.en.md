---
title: Backtester
keywords: wrds, backtest, backtester
description: "WRDS : using the Backtester too."
weight: 0
---



## Access

{{%tabs%}}
{{%tab "Instructions"%}}

1. Log into [wrds](../../connection/username).
2. Click on `Classroom > Full Catalog` an choose `Backtester` from the list.
3. Click on `Link to Platform`.

{{%/tab%}}

{{%tab "WRDS"%}}
![WRDS Backtester Create New](backtester_new.gif)
{{%/tab%}}

{{%/tabs%}}

## New Backtest

{{%tabs%}}
{{%tab "Instructions"%}}

1. Click on `New Backtest`
2. Choose the desired parameters for your backtest.
3. Save your settings by clicking on `Save New Backtest`.
4. You can access the corresponding charts and data from the `Backtester Dashboard` tab.

{{%notice tip%}}
Click on the question mark to know more about each available field.
{{%/notice%}}

{{%/tab%}}

{{%tab "WRDS"%}}
![WRDS Backtester Demo](backtester_demo.gif)
{{%/tab%}}

{{%/tabs%}}
