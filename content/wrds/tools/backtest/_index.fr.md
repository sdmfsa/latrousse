---
title: Backtester
keywords: wrds, backtest, backtester
description: "WRDS : exemple d'utilisation de l'outil Backtest."
weight: 0
---



## Accès

{{%tabs%}}
{{%tab "Instructions"%}}

1. Connectez-vous à [wrds](../../connection/username).
2. Cliquez sur `Classroom > Full Catalog` et ensuite sélectionnez `Backtester` dans la liste.
3. Cliquez sur `Link to Platform`.

{{%/tab%}}

{{%tab "WRDS"%}}
![WRDS Backtester Create New](backtester_new.gif)
{{%/tab%}}

{{%/tabs%}}

## Nouveau Backtest

{{%tabs%}}
{{%tab "Instructions"%}}

1. Cliquez sur `New Backtest`
2. Sélectionnez les paramètres du *backtest*.
3. Enregistrez votre *backtest* en cliquant sur `Save New Backtest`.
4. Utilisez les boutons de l'onglet `Backtester Dashboard` pour accéder aux graphiques ainsi que pour télécharger les données correspondantes.

{{%notice tip%}}
Cliquez sur le symbole de question pour avoir plus de détails sur les champs disponibles.
{{%/notice%}}

{{%/tab%}}

{{%tab "WRDS"%}}
![WRDS Backtester Demo](backtester_demo.gif)
{{%/tab%}}

{{%/tabs%}}
