---
title: Trouver un tableau
keywords: librairies wrds, nom des tableaux, séries de données
description: "WRDS : trouver le nom d'un tableau avec SAS Studio"
weight: 0
---

## Nom d'un tableau

Il est possible de visualiser les données disponibles sur WRDS via SAS/Studio.

{{%notice info%}}
Plusieurs bases de données son disponibles via WRDS, pourtant seul les données disponibles sous l'onglet `Subscriptions` à la page d'accueil sont accessibles.
{{%/notice%}}

1. Cliquez sur `Libraries` dans le menu gauche et faites un double clic sur le tableau `FRB > FX_MONTHLY`.

![](click_lib.png)

2. D'autres détails sur le tableau sont disponibles en cliquant sur le bouton `Table Properties`.

![](sas_options.png)

3. Pour créer des `filtres` la syntaxe `SQL` peut être utilisée de la même façon que dans une ligne `WHERE` de la procédure `SAS SQL` <a href="https://support.sas.com/documentation/cdl//en/sqlproc/69822/HTML/default/viewer.htm#titlepage.htm" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a> .

![](sas_filter.png)

4. SAS/Studio permet d'exporter le code `SAS` utilisé pour créer le tableau.
![](sas_export_code.png)

5. L'output est un fichier `.sas` qui peut être exécuté pour recréer le tableau. Dans cet exemple, le fichier se trouvera sous `/home/ulaval/votreutilisateur/example/mon_program.sas`.

![](sas_save_code.png)

## Fichier `.sas`

1. Ouvrez le fichier `mon_program.sas` et cliquez sur le bouton `Run all or selected code (F3)` ou appuyez sur `F3`.

![](sas_run_code.png)

2. Après l'exécution du programme, vous trouverez le journal sur l'onglet `LOG`, les résultats sur l'onglet `RESULTS` et les tableaux résultants sur l'onglet `OUTPUT DATA`.

![](sas_code_output.png)

## Téléchargement
Si vous avez besoin de sauvegarder une copie d'un tableau disponible sur `WRDS`, il est possible de le faire en cliquant sur `Export` après avoir fait un clic droit sur le nom du tableau dans le menu `Libraries`. Ensuite, il ne reste qu'à choisir le format désiré pour le téléchargement.

![](sas_export.png)

{{%notice tip%}}
Il est aussi possible de télécharger les résultats affichés après l'exécution d'un programme `SAS` en format `HTML`, `PDF` ou `WORD` via l'onglet `RESULTS`.
{{%/notice%}}
