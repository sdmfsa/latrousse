---
title: Web query
keywords: wrds, wrds ratios, financial ratios suite, web wrds, web query, query
description: "WRDS : sample queries using the web interface."
weight: 0
---

The following examples show you how to get data through the WRDS web interface.

> The instructions for creating a WRDS account are [here](../../connection/username).

## Financial ratios

{{%tabs%}}
{{%tab "Instructions"%}}

### Financial ratios

1. Click on `Financial Ratios Suite > Financial Ratios Industry Level`.
2. Complete the form as follows :
  + **Step 1** : `Date Range` from `1970-01` to `today`.
  + **Step 2** : `Universe` : `CRSP US Common Stocks`.
  + **Step 3** : `Industry Classification` : `GICS 10 sectors`.
  + **Step 4** : `Variables` : `All`.
  + **Step 5** : `Industry Aggregation` : `Median`.
  + **Step 6** : `Output Format` : `comma-delimited text (*.csv)`.
3. Save the output  `.csv` file by clicking on `Save link as...`.
4. Choose a folder to save the file on your computer, e.g. `Downloads`, and click on `Save`.


{{%/tab%}}

{{%tab "WRDS"%}}
![WRDS Financial Ratios Suite](wrds-ratios.gif)
{{%/tab%}}

{{%/tabs%}}

## CRSP

{{%tabs%}}
{{%tab "Instructions"%}}

### Historical prices

1. Click on `CRSP > Index / S&P 500 Indexes > CRSP Index File on the S&P 500`.
2. Complete the form as follows :
  + **Step 1** : `Date Range` from `1925-12` to `End date`. `Data Frequency : Monthly`.
  + **Step 2** : `Variables` : `Return on S&P Composite Index, Level on S&P 500 Composite Index`.
  + **Step 3** : `Output Format` : `comma-delimited text (*.csv)`.
3. Save the output  `.csv` file by clicking on `Save link as...`.
4. Choose a folder to save the file on your computer, e.g. `Downloads`, and click on `Save`.


{{%/tab%}}

{{%tab "WRDS"%}}
![WRDS CRSP](wrds-crsp.gif)
{{%/tab%}}

{{%/tabs%}}
