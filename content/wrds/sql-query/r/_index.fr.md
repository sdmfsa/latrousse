---
title: R
keywords: r sql, csv, exporter données r, format de date, dates, date r, wrds r, r wrds
description: "WRDS : Requête SQL via R."
weight: 0
---

Nous allons utiliser la syntaxe `SQL` pour appliquer des filtres et extraire certaines données du tableau `FRB.FX_MONTHLY` disponible sur `WRDS`.

{{%notice note%}}
Vous trouverez plus de détails sur le paquetage `RPostgres` soit dans la documentation officielle {{%extlink "https://github.com/r-dbi/RPostgres"%}} ou dans les manuels `PostgreSQL` {{%extlink "https://www.postgresql.org/docs/manuals/"%}}.
{{%/notice%}}

# Données sur les taux de change
L'exemple de requête ci-bas permet de créer le tableau `myforex` en choisissant les colonnes `date, excaus, exchus, exhkus, exinus, exjpus, exkous, exmxus` et `exukus` du tableau `FRB.FX_MONTHLY` lequel est disponible grâce à la connexion `wrds` créée avec le fichier `.Rprofile` {{%extlink "https://cran.r-project.org/web/packages/startup/vignettes/startup-intro.html"%}} . La connexion `wrds` nous permet donc d'accéder au tableau avec la syntaxe `FRB.FX_MONHTLY`.

```r
library(RPostgres)  # Connexion sur fichier ~/.Rprofile
myquery <- c(  # Syntaxe SQL
  "SELECT date , excaus , exchus , exhkus , exinus , exjpus ,
  exkous ,exmxus , exukus FROM FRB.FX_MONTHLY
  WHERE date > '2000-01-01'")

res <- dbSendQuery(wrds, myquery)
myforex <- dbFetch(res)  # L'argument n contrôle le nombre de lignes extraites
dbClearResult(res)

head(myforex, 5)  # Premières 5 observations
summary(myforex)
```

La ligne `WHERE` permet de filtrer pour n'obtenir que les données dont la valeur de la colonne `date` est supérieure à `2000-01-01` (**yyyy-mm-dd**).

{{% tabs %}}

{{% tab "Terminal" %}}
![](rquery_forex.gif)
{{% /tab %}}

{{% tab "RStudio" %}}
![](rquery_forex_rstudio.gif)
{{% /tab %}}

{{% /tabs %}}

# Changer le format des dates

Comme c'est le cas avec `Excel` et `SAS`, `R` sauvegarde les dates {{%extlink "https://www.stat.berkeley.edu/~s133/dates.html"%}} comme variables de type `numérique`. Pour contrôler comment la valeur de la colonne `date` est affichée, il suffit de modifier le `format` en conséquence <a href="https://stat.ethz.ch/R-manual/R-devel/library/base/html/format.html" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>. L'output sera une variable de type `character`.

```r
myforex$datestring <- format(myforex$date, "%d/%m/%Y")

head(myforex, 5)
```

L'exécution du code ci-haut change le `format` de la colonne `date` à `%d/%m/%Y` et sauvegarde le résultat dans une nouvelle colonne `datestring` .

{{% tabs %}}

{{% tab "Terminal" %}}
![](date_format.gif)
{{% /tab %}}

{{% tab "RStudio" %}}
![](date_format_rstudio.gif)
{{% /tab %}}

{{% /tabs %}}

# Exporter à CSV

Il est possible de créer une copie des données avec la fonction `write.csv` <a href="https://stat.ethz.ch/R-manual/R-devel/library/utils/html/write.table.html" target="_blank"><i class="fas fa-external-link-square-alt mb-0"></i></a>. L'exemple ci-bas sauvegarde une copie du tableau `myforex` comme `forex_prices.csv` dans le dossier `~/example`.

```r
write.csv(myforex, "~/example/forex_prices.csv")  # write.csv2 uses ";" as separator
```

{{% tabs %}}

{{% tab "Terminal" %}}
![](csv_export.gif)
{{% /tab %}}

{{% tab "RStudio" %}}
![](csv_export_rstudio.gif)
{{% /tab %}}

{{% /tabs %}}

# Exemple complet
Les commandes `R` ci-bas créeront trois fichiers `.csv` à partir de trois tableaux `wrds` à l'aide du paquetage `RPostgres`. Les fichiers résultats contiennent les taux de change obtenus à partir du tableau `FRB.FX_MONHTLY`, les prix des indices FTSE du tableau `COMP.G_IDX_MTH` et leurs noms qui se trouvent sur le tableau `COMP.G_NAMES_IX`.

{{% tabs %}}

{{% tab "Terminal" %}}
![](full.gif)
{{% /tab %}}

{{% tab "RStudio" %}}
![](full_rstudio.gif)
{{% /tab %}}

{{% /tabs %}}

Les fichiers `.csv` résultants peuvent être ouverts à partir de tout logiciel.
![](csv_view.png)

## Code R

{{% tabs %}}
{{% tab "forex_prices.csv" %}}

1. Créez le tableau `myforex` :

```r
library(RPostgres)
myquery <- c(
  "SELECT date , excaus , exchus , exhkus , exinus , exjpus ,
  exkous ,exmxus , exukus FROM FRB.FX_MONTHLY
  WHERE date > '2000-01-01'")

res <- dbSendQuery(wrds, myquery)
myforex <- dbFetch(res)
dbClearResult(res)
```

2. Modifiez le `format` de la colonne `date` au besoin :

```r
myforex$date <- format(myforex$date, "%d/%m/%Y")
```

3. Sauvegardez le tableau `myforex` comme `forex_prices.csv` :

```r
write.csv(myforex, "~/example/forex_prices.csv")
```

{{% /tab %}}

{{% tab "index_names.csv" %}}

1. Créez le tableau `gvnames` :

```r
library(RPostgres)
myquery <- c(
  "SELECT * FROM COMP.G_NAMES_IX WHERE gvkeyx IN ('150004', '150979', '150984', '150994')")

res <- dbSendQuery(wrds, myquery)
gvnames <- dbFetch(res)
dbClearResult(res)
```

2. Sauvegardez le tableau `gvnames` comme `index_names.csv` :

```r
write.csv(gvnames, "~/example/index_names.csv")
```
{{% /tab %}}

{{% tab "index_prices.csv" %}}


1. Créez le tableau `myindex` :

```r
library(RPostgres)
myquery <- c(
  "SELECT gvkeyx, datadate, prccm, prchm, prclm
  FROM COMP.G_IDX_MTH
  WHERE gvkeyx IN ('150004', '150979', '150984', '150994')
  AND datadate > '2000-01-01'")

res <- dbSendQuery(wrds, myquery)
myindex <- dbFetch(res)
dbClearResult(res)
```

2. Sauvegardez le tableau `myindex` comme `index_prices.csv` :

```r
write.csv(myindex, "~/example/index_prices.csv")
```

{{% /tab %}}

{{% /tabs %}}


