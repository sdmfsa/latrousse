---
title: R
keywords: wrds, wrds R, R, connexion R, requête avec R, connexion wrds
description: "WRDS : Comment se connecter via R."
weight: 0
---

{{%notice note%}}
Vous pouvez télécharger `R` sur le site officiel à l'adresse `https://www.r-project.org/`.
{{%/notice%}}

# Connexion

1. Lancez `R` et installez le paquetage `RPostgres`. Pour suivre ces instructions, vous pouvez utiliser l'interface graphique `R GUI` {{%extlink "https://www.ssc.wisc.edu/~hemken/Rworkshops/interface/RConsole.html"%}} (installée par défaut), `RStudio` {{%extlink "https://rstudio.com/products/rstudio/download/"%}} (un [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment) très populaire) ou le service `WRDS Cloud` {{%extlink "https://wrds-www.wharton.upenn.edu/pages/support/programming-wrds/programming-r/"%}} .

{{% tabs %}}

{{% tab "Terminal" %}} ![](install_rpostgres.gif) {{% /tab %}}
{{% tab "RStudio" %}} ![](install_rpostgres_rstudio.gif) {{% /tab %}}

{{% /tabs %}}

2. Créez un nouveau fichier, nommez-le `.pgpass` et sauvegardez ce fichier dans le dossier `home`. Le ficher doit avoir la ligne suivante où vous devez remplacer `wrds_username` et `wrds_password` par votre nom d'utilisateur et votre mot de passe WRDS.


```
wrds-pgdata.wharton.upenn.edu:9737:wrds:wrds_username:wrds_password
```

{{%notice info%}}
L'emplacement de votre dossier `home` ou `~/` dépend de votre système d'exploitation. Sur des systèmes de type Unix, ce dossier se trouve à `/Users/votreutilisateur` (sur MAC) ou à `/home/votreutilisateur`. Sur Windows, il s'agit du dossier `%APPDATA%` qui se trouve à un endroit semblable à l'adresse `C:\Users\yourusername\AppData\Roaming`.
{{%/notice%}}

{{% tabs %}}

{{% tab "Terminal" %}}
![](pgpass.gif)
{{% /tab %}}

{{% tab "Éditeur de texte" %}}
![](pgpass_editor.gif)
{{%notice note%}}
Sur Windows, évitez d'utiliser le Bloc-notes car vous devrez portez une attention accrue à l'extension du fichier créé. Au lieu du Bloc-notes, essayez [Notepad++](https://notepad-plus-plus.org/downloads/) (licence *open source*) ou [Sublime Text](https://www.sublimetext.com/) (licence propriétaire) .
{{%/notice%}}
{{% /tab %}}

{{% /tabs %}}

3. Utilisez la commande `chmod 600 ~/.pgpass` pou modifier les autorisations du fichier `.pgpass` tel qu'indiqué dans la documentation de `PostreSQL` {{%extlink "https://www.postgresql.org/docs/9.3/libpq-pgpass.html"%}} .
![](chmod.gif)

{{%notice info%}}
Ignorez ce pas si vous travaillez sur Windows.
{{%/notice%}}

4. Vous pouvez maintenant créer une connexion à `wrds` à l'aide du paquetage `RPostgres`. Pour ce faire, il suffit de remplacer `wrds_username` par votre nom d'utilisateur.

```r
library(RPostgres)
wrds <- dbConnect(Postgres(),
                  host='wrds-pgdata.wharton.upenn.edu',
                  port=9737,
                  dbname='wrds',
                  sslmode='require',
                  user='wrds_username')  # Mettre à jour
```

{{%notice tip%}}
Si vous désirez vous connecter automatiquement à WRDS à chaque fois que vous travaillez sur `R`, il suffit de sauvegarder le code ci-haut (avec votre `wrds_username`) dans votre [fichier](https://cran.r-project.org/web/packages/startup/vignettes/startup-intro.html) `.Rprofile`.
{{%/notice%}}

Essayez d'exécuter la requête `SQL` suivante pour tester votre connexion :

```r
res <- dbSendQuery(wrds, "SELECT date,dji FROM djones.djdaily")
data <- dbFetch(res, n=5)
dbClearResult(res)
```

Vous devriez retrouver les `5` premières lignes du tableau `djones.daily` dans la variable `data`.

{{% tabs %}}
  {{% tab "Terminal" %}}
  ![](test_wrds.gif)
  {{% /tab %}}

  {{% tab "RStudio" %}}
  ![](test_wrds_rstudio.gif)
  {{%notice info%}}
  La connexion à la base de données `wrds` a été créée automatiquement à l'aide du fichier `.Rprofile` {{%extlink "https://cran.r-project.org/web/packages/startup/vignettes/startup-intro.html"%}}.
  {{%/notice%}}
  {{% /tab %}}

{{% /tabs %}}


{{%notice tip%}}
Plusieurs exemples de requête sont disponibles sur le site web du service WRDS {{%extlink "https://wrds-www.wharton.upenn.edu/pages/support/programming-wrds/programming-r/querying-wrds-data-r/"%}} .
{{%/notice%}}


