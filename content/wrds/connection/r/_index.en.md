---
title: R
keywords: r, wrds, wrds, r, r query, wrds connection, r connection, connection
description: "WRDS : How to connect via R."
weight: 0
---

{{%notice note%}}
You can download `R` by visiting the official website at `https://www.r-project.org/`.
{{%/notice%}}

# Connection

1. Launch `R` and install the `RPostgres` package. You can use either the `R GUI` {{%extlink "https://www.ssc.wisc.edu/~hemken/Rworkshops/interface/RConsole.html"%}} (installed by default), `RStudio` {{%extlink "https://rstudio.com/products/rstudio/download/"%}} (widely used [IDE](https://en.wikipedia.org/wiki/Integrated_development_environment)) or the `WRDS Cloud` {{%extlink "https://wrds-www.wharton.upenn.edu/pages/support/programming-wrds/programming-r/"%}} to follow these instructions.

{{% tabs %}}

{{% tab "Terminal" %}} ![](install_rpostgres.gif) {{% /tab %}}
{{% tab "RStudio" %}} ![](install_rpostgres_rstudio.gif) {{% /tab %}}

{{% /tabs %}}

2. Create a file named `.pgpass` and save it in your `home` folder. The file must have the following line where you need to replace `wrds_username` and `wrds_password` with your WRDS user name and password.

```
wrds-pgdata.wharton.upenn.edu:9737:wrds:wrds_username:wrds_password
```

{{%notice info%}}
Your `home` or `~/` folder's locations depends on your operating system. On Unix-like systems it's located at `/Users/yourusername` (MAC) or `/home/yourusername`, on Windows it's called the `%APPDATA%` folder and it's located at something similar to `C:\Users\yourusername\AppData\Roaming`.
{{%/notice%}}

{{% tabs %}}

{{% tab "Terminal" %}}
![](pgpass.gif)
{{% /tab %}}

{{% tab "Text Editor" %}}
![](pgpass_editor.gif)
{{%notice note%}}
On Windows, avoid using the well known Notepad because you need to pay extra attention to its default extension settings. Instead, you could try [Notepad++](https://notepad-plus-plus.org/downloads/)(open-source license) our [Sublime Text](https://www.sublimetext.com/) (proprietary license).
{{%/notice%}}
{{% /tab %}}

{{% /tabs %}}

3. Use the command `chmod 600 ~/.pgpass` to modify the file permissions as explained in the `PostgreSQL` documentation {{%extlink "https://www.postgresql.org/docs/9.3/libpq-pgpass.html"%}} .
![](chmod.gif)

{{%notice info%}}
Ignore this step if working on Windows.
{{%/notice%}}

4. You can now create a connection to the `wrds` database using the `RPostgres` package by replacing your `wrds_username` in the code below :

```r
library(RPostgres)
wrds <- dbConnect(Postgres(),
                  host='wrds-pgdata.wharton.upenn.edu',
                  port=9737,
                  dbname='wrds',
                  sslmode='require',
                  user='wrds_username')  # Update
```

{{%notice tip%}}
Saving the above lines of code (using your `wrds_username`) in your `.Rprofile` [file](https://cran.r-project.org/web/packages/startup/vignettes/startup-intro.html) will create a connection to the `wrds` database every time your start a new `R` session.
{{%/notice%}}

You can also try the following `SQL` query to test your connection :

```r
res <- dbSendQuery(wrds, "SELECT date,dji FROM djones.djdaily")
data <- dbFetch(res, n=5)
dbClearResult(res)
```

You should find the first `5` lines of the `djones.daily` dataset in the `data` variable.

{{% tabs %}}
  {{% tab "Terminal" %}}
  ![](test_wrds.gif)
  {{% /tab %}}

  {{% tab "RStudio" %}}
  ![](test_wrds_rstudio.gif)
  {{%notice info%}}
  The connection to the `wrds` database was created automatically by using the `.Rprofile` {{%extlink "https://cran.r-project.org/web/packages/startup/vignettes/startup-intro.html"%}} file.
  {{%/notice%}}
  {{% /tab %}}

{{% /tabs %}}

{{%notice tip%}}
Additional query examples are available on the WRDS website {{%extlink "https://wrds-www.wharton.upenn.edu/pages/support/programming-wrds/programming-r/querying-wrds-data-r/"%}} .
{{%/notice%}}
