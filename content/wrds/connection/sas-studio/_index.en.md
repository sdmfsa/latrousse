---
title: SAS Studio
keywords: wrds, wrds sas, sas studio, sas online, sas wrds, sas
description: "WRDS : How to launch SAS Studio."
weight: 0
---

{{%notice info%}}
SAS/Studio is available through WRDS using any web browser. You will need to enter your WRDS credentials to log in.
{{%/notice%}}

# Connection

1. Log into your WRDS account at `https://wrds-www.wharton.upenn.edu`.
2. Click on `Get Data` and then on `SAS Studio`.
![](launch_sas.png)
3. Click on `Open SAS Studio`.
![](launch_sas_2.png)
4. Log in using your WRDS account.
![](login_sas.png)
5. You are now logged in and you will find your home folder at `/home/ulaval/yourusername`.
![](home_sas.png)
