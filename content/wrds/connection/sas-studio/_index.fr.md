---
title: SAS Studio
keywords: wrds, wrds sas, sas studio, sas en ligne, sas wrds, sas
description: "WRDS : Comment lancer SAS Studio."
weight: 0
---

{{%notice info%}}
SAS/Studio est disponible via WRDS à l'aide d'un navigateur web. Vous aurez besoin de votre compte WRDS pour vous connecter.
{{%/notice%}}

# Connexion

1. Connectez-vous à WRDS sur le site `https://wrds-www.wharton.upenn.edu`.
2. Cliquez sur `Get Data` et ensuite sur `SAS Studio`.
![](launch_sas.png)
3. Cliquez sur  `Open SAS Studio`.
![](launch_sas_2.png)
4. Connectez-vous à SAS/Studio avec le même compte WRDS.
![](login_sas.png)
5. Vous êtes maintenant connecté et votre dossier de travail se trouve à `/home/ulaval/votreutilisateur`.
![](home_sas.png)
